FROM tomcat:7

ENV JAVA_OPTS="-Doracle.net.tns_admin=$CATALINA_HOME"

RUN apt-get update && apt-get install curl 

RUN curl -o $CATALINA_HOME/tnsnames.ora http://service-oracle-tnsnames.web.cern.ch/service-oracle-tnsnames/tnsnames.ora

RUN rm -R $CATALINA_HOME/webapps/*

ADD ph-service-ess-reports.war $CATALINA_HOME/webapps/

RUN chgrp -R 0 $CATALINA_HOME && \
chmod -R g=u $CATALINA_HOME

USER 1001

EXPOSE 8080
CMD ["catalina.sh", "run"]
