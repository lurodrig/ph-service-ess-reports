package servlet.viewreport;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.URISyntaxException;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.URIResolver;
import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.commons.io.IOUtils;
import org.apache.commons.io.output.ByteArrayOutputStream;
import org.apache.fop.apps.FOUserAgent;
import org.apache.fop.apps.Fop;
import org.apache.fop.apps.FopFactory;
import org.apache.fop.render.awt.AWTRenderer;
import org.apache.fop.servlet.ServletContextURIResolver;
import org.xml.sax.SAXException;

import com.sun.org.apache.xml.internal.security.utils.resolver.ResourceResolver;
import com.sun.org.apache.xml.internal.utils.URI;

import ess.reports.Parameters;
import oracle.jdbc.OracleResultSet;
import oracle.sql.CLOB;

/**
 * Servlet implementation class ViewReport
 */
@WebServlet("/ViewReport")
public class ViewReport extends HttpServlet {
	  Connection conn = null ;
	    PreparedStatement PSgetXML = null  ;
	    String estring = "";
	    Parameters param = new Parameters();
	    String actionId = "";
	    String[] xmlData = null;    
	    
	    /** Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
	     * @param request servlet request
	     * @param response servlet response
	     * @throws SAXException 
	     * @throws TransformerException 
	     * @throws URISyntaxException 
	     */
	    
	    
	    @SuppressWarnings("unused")
		protected void processRequest(HttpServletRequest request, HttpServletResponse response)
	    throws ServletException, IOException, SAXException, TransformerException, URISyntaxException {
	        
	        HttpSession session = request.getSession(true);
	        
	        
	        if(session.getAttribute("conn") == null) {
	            try {
	                Class.forName("oracle.jdbc.OracleDriver").newInstance();
	                // Class.forName("org.gjt.mm.mysql.Driver").newInstance();
	                conn = DriverManager.getConnection(param.getTnsURL(), param.getUser(), param.getPassword());
	                PSgetXML = conn.prepareStatement(
	                        "select x.report.getClobVal(),extractvalue(report,'/report/@type') " +
	                        "from autotest_report x where action_id = ?");  
	                //MJ: "/report/@type" seems to extract the letter (currently A..I) that indicates the format of the XML data
	                session.setAttribute("conn",conn);
	                session.setAttribute("PSgetXML",PSgetXML);
	                session.setAttribute("binding.listener",new CustomBindingListener(conn));
	            } catch(Exception e) {
	                response.setContentType("text/html;charset=UTF-8");
	                StringWriter sw = new StringWriter();
	                PrintWriter pw = new PrintWriter(sw);
	                e.printStackTrace(pw);
	                OutputStreamWriter ow = new OutputStreamWriter(response.getOutputStream());
	                ow.write(sw.toString());
	                ow.close();
	                sw.close();
	                pw.close();
	                return ;
	            }
	            
	        } else {
	            conn = (Connection) session.getAttribute("conn");
	            PSgetXML = (PreparedStatement) session.getAttribute("PSgetXML");
	        }
	        //test request
	        String actionId = (String)request.getParameter("action_id");
	        System.out.println("actionId is:" + actionId);
	        xmlData = (String[])getXMLData((String)request.getParameter("action_id"));
	        System.out.println("the xml result is:"+ xmlData[0]);
	       
	        if(getXMLData(request.getParameter("action_id")).length == 0) {
	            response.setContentType("text/html;charset=UTF-8");
	            PrintWriter out = response.getWriter();
	            out.println("<html>");
	            out.println("<head>");
	            out.println("<title>Servlet ViewReport</title>");
	            out.println("</head>");
	            out.println("<body>");
	            out.println("<h1>Servlet ViewReport Error </h1>");
	            out.println("<h2> No Data </h2>");
	            out.println("<pre>");
	            out.println(estring);
	            out.println("</pre>");
	            out.println("</body>");
	            out.println("</html>");
	              
	            out.close();
	            return ;   
	        }
	        
	        String xstring = xmlData[0];
	        System.out.println(xstring);
	        String rtype = request.getParameter("report");
	        if(rtype == null) rtype = "" ;

	        String xslName = "WEB-INF/report" +xmlData[1] + rtype + "fo.xsl" ;
	        
	        //local directory for XSL stylesheets retrieval
	        String localXSLpath = ".\\WebContent/";
	        
	        //servlet directory for XSL stylesheets retrieval
	          String servletPath = "servlet-context:/";
	        
	        String fn = localXSLpath + xslName;
	          
	        //String fn = servletPath + xslName;
	          
	          //print out message of the selected XSL file and full path
		        System.out.println("xsl files path:" + fn);
		        
		        //constructing the XSL resource
		        File xslfile = new File(xslName);
	        
	        
	        //**Z.Alg 15/06/16 -- old code**// 
	        //FileOutputStream out = new FileOutputStream("errorslog.txt");
	        //String relativePDFPath = "/WebContent/myfile.pdf";
	        //FileOutputStream out = new FileOutputStream(new File(getServletContext().getRealPath(relativePDFPath)));
	        //**Z.Alg 15/06/16 -- old code -- end of 1st region**// 
		        
	        {
	        	String relativeWebPath = "/WEB-INF/fopxconf.xml";
	 	        String filepath = getServletContext().getRealPath(relativeWebPath);
	 	        File f = new File(getServletContext().getRealPath(relativeWebPath));
	 	        //File tempFile = getTempFile(filepath);
	 	       TransformerFactory transFactory = TransformerFactory.newInstance();        	
	 	       transFactory = TransformerFactory.newInstance();
	 	       transFactory.setURIResolver(this.uriResolver);
	 	       
	 		    ResourceResolver resolver = new ResourceResolver(null){
	 		    	  public OutputStream getOutputStream(URI uri) throws IOException {
	 		              URL url = getServletContext().getResource(uri.toString());
	 		              return url.openConnection().getOutputStream();
	 		          }	 		    	
	 		    };

	 	       
	 	        //System.out.println(tempFile);
	  	       
	 	       
	 	        //String absoluteDiskPath = getServletContext().getRealPath(relativeWebPath);
	 	        InputStream confStream = getClass().getClassLoader().getResourceAsStream("fopxconf.xml");
	 	        
	 	        String localFilePath = "/WebContent/WEB-INF/fop.xconf";
	 	        String filePath = "servlet-context:/WEB-INF/fop.xconf";
	 	        //File fopFile = (File) this.uriResolver.resolve(filePath, null);
	 	       System.out.println("file is:" + localFilePath);
	 	      FopFactory fopFactory = FopFactory.newInstance(getServletContext().getResource("/WEB-INF/fop.xconf").toURI());
//	            FopFactory fopFactory = FopFactory.newInstance(new File(localFilePath));
	             
	             //Setup output
	 	        ByteArrayOutputStream fileout = new ByteArrayOutputStream();
	 	        /*String relativePDFPath = "/WebContent/myfile.pdf";
	 	        FileOutputStream out = new FileOutputStream(new File(getServletContext().getRealPath(relativePDFPath)));*/
	 	        //Setup Fop User Agent
	 	        FOUserAgent foUserAgent = fopFactory.newFOUserAgent(); 
	 	        //Setup renderer
	 	        AWTRenderer renderer = new AWTRenderer(foUserAgent);
	 	        foUserAgent.setRendererOverride(renderer);
	        	
	        	final Fop fop = fopFactory.newFop("application/pdf", fileout);
	        	
	        	//xsl resource for the servlet
	            //Source xslsrc = this.uriResolver.resolve(fn, null);
	            
	        	//local xsl resource syntax
	            Source localXslsrc = new StreamSource(getServletContext().getResourceAsStream(xslName));
	            
	        	Transformer transformer = transFactory.newTransformer(localXslsrc);
	        	transformer.setURIResolver(this.uriResolver);
	        	//Make sure the XSL transformation's result is piped through to FOP
	        	Result result = new SAXResult(fop.getDefaultHandler());
	        	
	        	Source xmlsrc = new StreamSource(new StringReader(xstring));
	            transformer.transform(xmlsrc, result);
	            System.out.println("generated pdf has {} pages" + fop.getResults().getPageCount());    
	              
	            //Prepare response
	 	          response.setContentType("application/pdf");
	 	          response.setContentLength(fileout.size());
	 	          System.out.println("file's size is:" + fileout.size());
	 	          

	 	          //Send content to Browser
	 	          response.getOutputStream().write(fileout.toByteArray());
	 	          response.getOutputStream().flush();
	               
	 	   
	        }
	      /*  catch(TransformerConfigurationException tc) {
	            response.setContentType("text/html;charset=UTF-8");
	            StringWriter sw = new StringWriter();
	            PrintWriter pw = new PrintWriter(sw);
	            tc.printStackTrace(pw);
	           
	            sw.close();
	            pw.close();
	        } catch(TransformerException te) {
	            response.setContentType("text/html;charset=UTF-8");
	            StringWriter sw = new StringWriter();
	            PrintWriter pw = new PrintWriter(sw);
	            te.printStackTrace(pw);
	           
	            sw.close();
	            pw.close();
	        }*/
	        //out.close();
	       
	    }
	    
	    
	    protected URIResolver uriResolver;
	    
	    public void init() throws ServletException{
	    	this.uriResolver = new ServletContextURIResolver(getServletContext());
	    }
	    
	    public File getTempFile(String filepath) throws IOException {
	    	InputStream input = getClass().getResourceAsStream(filepath);
 	        File tempfile = File.createTempFile("foptemp", ".tmp");
 	        tempfile.deleteOnExit();
 	        FileOutputStream output = new FileOutputStream(tempfile);
 	        IOUtils.copy(input, output);
	    	return tempfile;
	    }
	    
	    public String[] getXMLData(String id) throws IOException {
	        
	        try {
	            PSgetXML.setString(1,id);
	            ResultSet rs = PSgetXML.executeQuery();
	            if(!rs.next()) return null;
	            CLOB clob = ((OracleResultSet)rs).getCLOB(1);
//	            CLOB clob = (CLOB) rs.getObject(1);
	            Reader r = clob.getCharacterStream();
	            char[] buf = new char[50];
	            StringBuffer sb = new StringBuffer();
	            int nch = 0;
	            try {
	                while((nch = r.read(buf)) != -1) sb.append(buf,0,nch);
	            } catch (IOException e1) {
	                e1.printStackTrace();
	                return null;
	            }
	            String[] val = new String[2];
	            val[0] = sb.toString();
	            val[1] = rs.getString(2);
	            rs.close();
	            return val;
	        } catch (SQLException e) {
	            StringWriter sw = new StringWriter();
	            PrintWriter pw = new PrintWriter(sw);
	            e.printStackTrace(pw);
	            estring =sw.toString();
	            sw.close();
	            pw.close();
	        } catch (Exception e2) {
	            StringWriter sw = new StringWriter();
	            PrintWriter pw = new PrintWriter(sw);
	            e2.printStackTrace(pw);
	            estring =sw.toString();
	            sw.close();
	            pw.close();
	            
	        }
	        
	        return null;
	    }
	    
	    
	    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
	    /** Handles the HTTP <code>GET</code> method.
	     * @param request servlet request
	     * @param response servlet response
	     */
	    protected void doGet(HttpServletRequest request, HttpServletResponse response)
	    throws ServletException, IOException {
	        try {
				processRequest(request, response);
			} catch (SAXException | TransformerException | URISyntaxException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    }
	    
	    /** Handles the HTTP <code>POST</code> method.
	     * @param request servlet request
	     * @param response servlet response
	     */
	    protected void doPost(HttpServletRequest request, HttpServletResponse response)
	    throws ServletException, IOException {
	        try {
				processRequest(request, response);
			} catch (SAXException | TransformerException | URISyntaxException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    }
	    
	    /** Returns a short description of the servlet.
	     */
	    public String getServletInfo() {
	        return "Short description";
	    }
	    // </editor-fold>
	    
	    class CustomBindingListener implements HttpSessionBindingListener {
	        
	        Connection saveconn ;
	        
	        CustomBindingListener(Connection conn) {
	            saveconn = conn ;
	        }
	        
	        public void valueBound(HttpSessionBindingEvent event) {}
	        
	        public void valueUnbound(HttpSessionBindingEvent event) {
	            
	            try {
	                saveconn.close();
	            } catch (SQLException e) {}
	            
	        }
	    }
}
