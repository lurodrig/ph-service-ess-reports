package ess.reports;

public class Parameters {

	// this is the connection String used only for connecting to the database
	// via the local server
	// should replace the return parameter tnsURL in the getURL() method below
	// while running the application locally.
	private static String url = "jdbc:oracle:thin:@(DESCRIPTION=(ADDRESS= "
			+ "(PROTOCOL=TCP) (HOST=devdb11-s.cern.ch) (PORT=10121) )"
			+ "(ENABLE=BROKEN)(CONNECT_DATA=(SERVICE_NAME=devdb11_s.cern.ch))";

	// the thsName connection String used instead of the hardcoded connection
	// String
	private static String tnsURL = System.getenv("DB_URL");

	private static String user = System.getenv("DB_USER");
	private static String password = System.getenv("DB_PASSWORD");

	/** Creates a new instance of Parameters */
	public Parameters() {
	}

	public String getURL() {
		return url;
	}

	public String getUser() {
		return user;
	}

	public String getPassword() {
		return password;
	}

	public String getTnsURL() {
		return tnsURL;
	}
}
