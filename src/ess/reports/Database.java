package ess.reports;

import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;

import oracle.jdbc.pool.OracleDataSource;
//TODO: Create Parameters class import ReportsWeb.ess.reports.Parameters;
import oracle.sql.CLOB;

public class Database implements HttpSessionBindingListener {

	  Connection conn = null;
 	  Connection connection = null;

	  PreparedStatement PSgetXML = null;
	  Parameters param = new Parameters();  

	  
	  /** Creates a new instance of Database 
	 * @throws SQLException 
	 * @throws ClassNotFoundException */
	  //**************/
	  public Database() throws SQLException, ClassNotFoundException
	  //**************/
	  {   
	   	//local tnsnames.ora destination file
    	//System.setProperty("oracle.net.tns_admin", "C:/Oracle/InstantClient");
		Class.forName("oracle.jdbc.OracleDriver");
		System.out.println("Database: in constructor");	
	    try
	    { 
	    	//new code - region start
	    	
	    	conn = DriverManager.getConnection(param.getTnsURL(), param.getUser(), param.getPassword());
	    	PSgetXML = conn.prepareStatement("select x.report.getClobVal(),extractvalue(report,'/report/@type') from autotest_report x where action_id = ?");
		    System.out.println("Database: constructor done");

	    	
	    	//new code - region stop
	    	
	   
	    }
	    catch(Exception e)
	    {
	      System.out.println("Database: Error from newInstance");
	      e.printStackTrace();
	      //return;
	      System.exit(-1);
	    }

	    System.out.println("Database: newInstance OK");
	    
	    try
	    {
	    	conn = DriverManager.getConnection(param.getURL(), param.getUser(), param.getPassword());
	    	PSgetXML = conn.prepareStatement("select x.report.getClobVal(),extractvalue(report,'/report/@type') from autotest_report x where action_id = ?");
		    System.out.println("Database: constructor done");
	    
	    }
	    catch(Exception e)
	    {
	      e.printStackTrace();
	      return;
	    }
	  }

	  
	  //************************/
	  public String[][] getList()
	  //************************/
	  {    
	    int colcount;
	    System.out.println("Database:getList: called");
	    
	    try
	    {
	      PreparedStatement PSgetList = null;

	      String db_query = "select action_id,extractvalue(report,'/report/mfserial[position()=1]'),extractvalue(report,'/report/date[position()=1]'),extractvalue(report,'/report/time[position()=1]') from autotest_report order by action_id";
	      
	      
	      System.out.println("Database:getList: DB query = " + db_query);

	      System.out.println("Database:getList: accessing DB");
	      PSgetList = conn.prepareStatement(db_query);
	      System.out.println("Database:getList: DB OK");
	      
	      ResultSet rs = PSgetList.executeQuery();
	      
	      
	      colcount = 4;
	      ArrayList valsL = new ArrayList();
	      
	      while(rs.next())
	      {
	        String[] item = new String[colcount];
	        for(int j = 0; j < colcount; j++)
	        {
	          item[j] = rs.getString(j + 1);
	        }
	        valsL.add(item);
	      }
	      String[][] vals = (String[][]) valsL.toArray(new String[0][0]);
	      rs.close();
	      return vals;
	    }
	    catch (SQLException e)
	    {
	      System.out.println("SQLException:   " + e.getMessage());
	      System.out.println("SQLState:       " + e.getSQLState());
	      System.out.println("VendorError:    " + e.getErrorCode());
	    }
	    return null;
	  }
	  
	  
	  //**********************************/
	  public String[] getXMLData(String id)
	  //**********************************/
	  {    
	    System.out.println("Database:getXMLData called");
	    try
	    {
	      PSgetXML.setString(1, id);
	      ResultSet rs = PSgetXML.executeQuery();
	      System.out.println("Resultset is:" + rs);
	      if(!rs.next()) 
	        return null;
	      Clob clob = rs.getClob(1);
	      Reader r = clob.getCharacterStream();
	      char[] buf = new char[50];
	      StringBuffer sb = new StringBuffer();
	      int nch = 0;
	      try
	      {
	        while((nch = r.read(buf)) != -1) sb.append(buf,0,nch);
	      }
	      catch (IOException e1)
	      {
	        e1.printStackTrace();
	        return null;
	      }
	      String[] result = new String[2];
	      result[0] = sb.toString();
	      result[1] = rs.getString(2);
	      rs.close();
	      return result;
	    }
	    catch (SQLException e)
	    {
	      System.out.println("SQLException:   " + e.getMessage());
	      System.out.println("SQLState:       " + e.getSQLState());
	      System.out.println("VendorError:    " + e.getErrorCode());
	    }
	    
	    return null;
	  }
	  

	  
	  //**************************************************/
	  public void valueBound(HttpSessionBindingEvent event)
	  //**************************************************/
	  {}
	  
	  
	  //****************************************************/
	  public void valueUnbound(HttpSessionBindingEvent event)
	  //****************************************************/
	  {  
	    System.out.println("Database:valueUnbound: called");
	    try
	    {
	      conn.close();
	    }
	    catch (SQLException e)
	    {
	      System.out.println("SQLException:   " + e.getMessage());
	      System.out.println("SQLState:       " + e.getSQLState());
	      System.out.println("VendorError:    " + e.getErrorCode());
	    } 
	  }
	
}
