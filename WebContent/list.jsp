<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<jsp:useBean id="db" class="ess.reports.Database" scope="session"></jsp:useBean>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>List of Tests</title>
    </head>
    <body>

    <h1 align="center">List of Tests</h1>
    
        <% String[][] vals = db.getList(); %>
    
    <div align="center">
      <table width="80%"  border="0">
        <tr>
          <th width="12%" scope="col"><div align="left">ID</div></th>
          <th width="29%" scope="col"><div align="left">manufserial</div></th>
          <th width="25%" scope="col"><div align="left">date</div></th>
          <th width="26%" scope="col"><div align="left">time</div></th>
        </tr>
        <% for(int i =0 ; i < vals.length ; i++) {  %>
        <tr>
          <td><a href=<%= "ViewReport?action_id=" + vals[i][0]  %>><%= vals[i][0]%></a></td>
          <td><%= vals[i][1]%></td>
          <td><%= vals[i][2]%></td>
          <td><%= vals[i][3]%></td>
        </tr>
        <% } %>
          </table>
    </div>
    <p align="center">&nbsp; </p>
    
    </body>
   
</html>
