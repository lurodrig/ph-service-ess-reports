<?xml version="1.0"?> 
 
 
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
	xmlns:fo="http://www.w3.org/1999/XSL/Format">
  <xsl:output method="xml" indent="yes"/>

 
 
 <xsl:template match="/" >

<fo:root > 

  <fo:layout-master-set>
  <!-- fo:layout-master-set defines in its children the page layout: 
       the pagination and layout specifications
      - page-masters: have the role of describing the intended subdivisions 
                       of a page and the geometry of these subdivisions 
                      In this case there is only a simple-page-master which defines the 
                      layout for all pages of the text
  -->
    <!-- layout information -->
    <fo:simple-page-master master-name="A4-first"
                  page-height="29.7cm" 
                  page-width="21cm"
                  margin-top="1cm" 
                  margin-bottom="2cm" 
                  margin-left="2.5cm" 
                  margin-right="2.5cm">
      <fo:region-body margin-top="3cm" margin-bottom="1.5cm" />
      <fo:region-before extent="3cm" region-name="first-header" />
      <fo:region-after extent="1.5cm"/>
    </fo:simple-page-master>
    <fo:simple-page-master master-name="A4-rest"
                  page-height="29.7cm" 
                  page-width="21cm"
                  margin-top="1cm" 
                  margin-bottom="2cm" 
                  margin-left="2.5cm" 
                  margin-right="2.5cm">
      <fo:region-body margin-top="3cm" margin-bottom="1.5cm"/>
      <fo:region-before extent="3cm" region-name="rest-header"/>
      <fo:region-after extent="1.5cm"/>
    </fo:simple-page-master>
    
    <fo:page-sequence-master master-name="my-sequence" >
    		<fo:single-page-master-reference master-reference="A4-first" />
    		<fo:repeatable-page-master-reference master-reference="A4-rest" />
    	</fo:page-sequence-master>
    
  </fo:layout-master-set>
  <!-- end: defines page layout -->
  
    <fo:page-sequence master-reference="my-sequence">
 
    <fo:static-content flow-name="first-header" font-family = "Times">
      <fo:block>
    <fo:external-graphic src="url('CERNLetterHead.gif')" content-width="18cm" />
  </fo:block>
 </fo:static-content>   
    
    <fo:static-content flow-name="rest-header" font-family = "Times">
  <fo:block font-size="18pt"
  		text-align="center"
  		font-weight="bold">
  		Test Report
  </fo:block>
 </fo:static-content>   
    
    
   <fo:flow flow-name="xsl-region-body" font-size="12pt" 
            font-family="Times">
   
 	<xsl:apply-templates />
    </fo:flow> <!-- closes the flow element-->
  </fo:page-sequence> <!-- closes the page-sequence -->
</fo:root>
 </xsl:template>

 
 <xsl:template match="report[@type='root']">
 
 <!-- 	This template matches the root report
 		it must put out the header and identification information
 		and call the lower templates
   -->
 
  
   <fo:block font-size="18pt"
  		text-align="center"
  		font-weight="bold">
  		Test Report
  </fo:block>
 
  <fo:block font-size="14pt"
  		text-align="center"
  		font-weight="bold">
  		VME Power Supplies
  </fo:block>
 
 
 <fo:table table-layout="fixed" width = "100%" padding-before = "1cm">
<fo:table-column column-width = "12cm" />
<fo:table-column column-width = "3cm" />

<fo:table-body>

  <fo:table-row>
    <fo:table-cell>
 
 
   
 <fo:table table-layout="fixed" width = "100%" >
<fo:table-column  />
<fo:table-column  />

<fo:table-body>

  <fo:table-row>
    <fo:table-cell>
      <fo:block font-size="12pt" >
		Manufacturer's Serial Number
		</fo:block>
    </fo:table-cell>

   <fo:table-cell>
      <fo:block font-size="12pt" >
		<xsl:value-of select="vdesc[text()='mfserial']/val" />
		</fo:block>
    </fo:table-cell>
 </fo:table-row>
    
  <fo:table-row>
    <fo:table-cell>
      <fo:block font-size="12pt" >
		Type
		</fo:block>
    </fo:table-cell>

   <fo:table-cell>
      <fo:block font-size="12pt" >
		<xsl:value-of select="vdesc[text()='mftype']/val" />
		</fo:block>
    </fo:table-cell>
 </fo:table-row>
    
  <fo:table-row>
    <fo:table-cell>
      <fo:block font-size="12pt" >
		LHC Crate Serial Number
		</fo:block>
    </fo:table-cell>

   <fo:table-cell>
      <fo:block font-size="12pt" >
		<xsl:value-of select="vdesc[text()='lhcid']/val" />
		</fo:block>
    </fo:table-cell>
 </fo:table-row>
    
  <fo:table-row>
    <fo:table-cell>
      <fo:block font-size="12pt" >
		Pool Serial Number
		</fo:block>
    </fo:table-cell>

   <fo:table-cell>
      <fo:block font-size="12pt" >
		<xsl:value-of select="vdesc[text()='poolid']/val" />
		</fo:block>
    </fo:table-cell>
 </fo:table-row>
    
  <fo:table-row>
    <fo:table-cell>
      <fo:block font-size="12pt" >
		Operator
		</fo:block>
    </fo:table-cell>

   <fo:table-cell>
      <fo:block font-size="12pt" >
		<xsl:value-of select="vdesc[text()='operator']/val" />
		</fo:block>
    </fo:table-cell>
 </fo:table-row>
    
  <fo:table-row>
    <fo:table-cell>
      <fo:block font-size="12pt" >
		Date
		</fo:block>
    </fo:table-cell>

   <fo:table-cell>
      <fo:block font-size="12pt" white-space-collapse="false">
		<xsl:value-of select="vdesc[text()='date']/val" /> - <xsl:value-of select="vdesc[text()='time']/val" />
		</fo:block>
    </fo:table-cell>
 </fo:table-row>
    
  </fo:table-body>
</fo:table>
 
     </fo:table-cell>
   
      <fo:table-cell>
      <fo:block font-size="20pt"  font-weight="bold">
	<xsl:if test="@result='pass'">
	PASSED
	</xsl:if>
	<xsl:if test="@result='fail'">
	FAILED
	</xsl:if>
		</fo:block>
    </fo:table-cell>
     
     
 </fo:table-row>
    </fo:table-body>
</fo:table>

<xsl:apply-templates />
 
 </xsl:template> 
 
  <xsl:template match="report[@type='test']">
 
 <!-- 	This template matches a test
 		it may call list and table templates
    -->
 
 	<fo:block
 		font-size="14pt"
   		font-weight="bold"
		padding-before = "0.5cm">
		<xsl:value-of select = "vdesc[text()='desc']/val" />
		- <xsl:value-of select = "@result" />
	</fo:block>
		
		<xsl:apply-templates select = "result" />
		<xsl:apply-templates select = "report" />

 </xsl:template> 
 
 <xsl:template match = "result" >
 	<fo:block>
		<xsl:value-of select="text()" /> ===== <xsl:value-of select="desc" />
	</fo:block>
</xsl:template>
 
 
 <xsl:template match="report[@type='table']">

<fo:table table-layout="fixed" width = "100%" padding-before="0.5cm">

	<xsl:for-each select="vdesc/text()[1]">
		<fo:table-column />
	</xsl:for-each>
	<fo:table-body>
  		<fo:table-row>
			<xsl:for-each select="vdesc/text()[1]">
                  <fo:table-cell>
                  	<fo:block>
                  		<xsl:value-of select="." />
                  	</fo:block>
                  </fo:table-cell>
			</xsl:for-each>
		</fo:table-row>
                
		<xsl:call-template name="line">
			<xsl:with-param name="count" select="1" />
		</xsl:call-template>
 
    </fo:table-body>
</fo:table>

</xsl:template>

<xsl:template name = "line" >
	<xsl:param name = "count" select = "1" />
	<xsl:if test = "$count &lt;= count(vdesc[1]/val/text()[1])" >
		<fo:table-row>
		<xsl:for-each select="vdesc">
                  <fo:table-cell>
		<xsl:if test="./val[$count]/tag = 'f'">
			<fo:block font-weight="bold">
				<xsl:value-of select = "./val[$count]/text()" />
			</fo:block>
		</xsl:if>
		<xsl:if test="not(./val[$count]/tag)">
		<fo:block>
			<xsl:value-of select = "./val[$count]/text()" />
		</fo:block>
		</xsl:if>
		</fo:table-cell>
		</xsl:for-each>
		</fo:table-row>
		<xsl:call-template name="line">
			<xsl:with-param name="count" select="$count+1" />
		</xsl:call-template>
		
		</xsl:if>
</xsl:template>

 
 
</xsl:stylesheet>
 