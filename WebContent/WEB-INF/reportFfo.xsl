<?xml version="1.0"?> 








 
 
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
	xmlns:fo="http://www.w3.org/1999/XSL/Format">
  <xsl:output method="xml" indent="no"/>

<xsl:decimal-format  NaN="---" />
 
 
 <xsl:template match="/" >

<fo:root > 


  <fo:layout-master-set>
  <!-- fo:layout-master-set defines in its children the page layout: 
       the pagination and layout specifications
      - page-masters: have the role of describing the intended subdivisions 
                       of a page and the geometry of these subdivisions 
                      In this case there is only a simple-page-master which defines the 
                      layout for all pages of the text
  -->
    <!-- layout information -->
    <fo:simple-page-master master-name="A4-first"
                  page-height="29.7cm" 
                  page-width="21cm"
                  margin-top="1cm" 
                  margin-bottom="2cm" 
                  margin-left="2.5cm" 
                  margin-right="2.5cm">
      <fo:region-body margin-top="3cm" margin-bottom="1.5cm" />
      <fo:region-before extent="3cm" region-name="first-header" />
      <fo:region-after extent="1.5cm"/>
    </fo:simple-page-master>
    <fo:simple-page-master master-name="A4-rest"
                  page-height="29.7cm" 
                  page-width="21cm"
                  margin-top="1cm" 
                  margin-bottom="2cm" 
                  margin-left="2.5cm" 
                  margin-right="2.5cm">
      <fo:region-body margin-top="3cm" margin-bottom="1.5cm"/>
      <fo:region-before extent="3cm" region-name="rest-header"/>
      <fo:region-after extent="1.5cm" region-name="rest-footer"/>
    </fo:simple-page-master>
    
    <fo:page-sequence-master master-name="my-sequence" >
    		<fo:single-page-master-reference master-reference="A4-first" />
    		<fo:repeatable-page-master-reference master-reference="A4-rest" />
    	</fo:page-sequence-master>
    
  </fo:layout-master-set>
  <!-- end: defines page layout -->
  
    <fo:page-sequence master-reference="my-sequence">
 
    <fo:static-content flow-name="first-header" font-family = "Times">
      <fo:block>
    <fo:external-graphic src="url('CERNLetterHead.gif')" content-width="18cm" />
  </fo:block>
 </fo:static-content>   
    
    <fo:static-content flow-name="rest-header" font-family = "Times">
  <fo:block font-size="18pt"
  		text-align="center"
  		font-weight="bold">
  		Test of <xsl:value-of select = "//lhcid" /> performed on <xsl:value-of select = "//date" /> at <xsl:value-of select = "//time" />

  </fo:block>
 </fo:static-content>   
    
    <fo:static-content flow-name="rest-footer" font-family = "Times">
  <fo:block font-size="12pt"
  		text-align="center">
		<fo:page-number />
  </fo:block>
 </fo:static-content>   
    
    
   <fo:flow flow-name="xsl-region-body" font-size="12pt" 
            font-family="Times">
   
 	<xsl:apply-templates />
    </fo:flow> <!-- closes the flow element-->
  </fo:page-sequence> <!-- closes the page-sequence -->
</fo:root>
 </xsl:template>

 <xsl:template match="report">
 
 <!-- 	This template matches the root report
 		it must put out the header and identification information
 		and call the lower templates

		HEADERS
   -->
   
   <fo:block font-size="18pt"
  		text-align="center"
  		font-weight="bold">
  		Test report for crate power supply

  </fo:block>
 
  <fo:block font-size="14pt"
  		text-align="center"
  		font-weight="bold">
  		

  </fo:block>

<!--		Identification area -->

<fo:table table-layout="fixed" width = "100%" padding-before = "1cm">
<fo:table-column column-width = "12cm" />
<fo:table-column column-width = "3cm" />

<fo:table-body>

  <fo:table-row>
    <fo:table-cell>
 
 
   
 <fo:table table-layout="fixed" width = "100%" >
<fo:table-column  />
<fo:table-column  />

<fo:table-body>





  <fo:table-row>
    <fo:table-cell>
      <fo:block font-size="10pt" >
		Manufacturer's Serial Number
		</fo:block>
    </fo:table-cell>

   <fo:table-cell>
      <fo:block font-size="10pt" >
		<xsl:value-of select = "mfserial" />
		</fo:block>
    </fo:table-cell>
 </fo:table-row>


  <fo:table-row>
    <fo:table-cell>
      <fo:block font-size="10pt" >
		Manufacturer's Part Number
		</fo:block>
    </fo:table-cell>

   <fo:table-cell>
      <fo:block font-size="10pt" >
		<xsl:value-of select = "mftype" />
		</fo:block>
    </fo:table-cell>
 </fo:table-row>


  <fo:table-row>
    <fo:table-cell>
      <fo:block font-size="10pt" >
		Technical DB Serial Number
		</fo:block>
    </fo:table-cell>

   <fo:table-cell>
      <fo:block font-size="10pt" >
		<xsl:value-of select = "lhcid" />
		</fo:block>
    </fo:table-cell>
 </fo:table-row>


  <fo:table-row>
    <fo:table-cell>
      <fo:block font-size="10pt" >
		Operator
		</fo:block>
    </fo:table-cell>

   <fo:table-cell>
      <fo:block font-size="10pt" >
		<xsl:value-of select = "operator" />
		</fo:block>
    </fo:table-cell>
 </fo:table-row>


  <fo:table-row>
    <fo:table-cell>
      <fo:block font-size="10pt" >
		Date
		</fo:block>
    </fo:table-cell>

   <fo:table-cell>
      <fo:block font-size="10pt" >
		<xsl:value-of select = "date" /> - <xsl:value-of select = "time" />
		</fo:block>
    </fo:table-cell>
 </fo:table-row>


  <fo:table-row>
    <fo:table-cell>
      <fo:block font-size="10pt" >
		Uptime
		</fo:block>
    </fo:table-cell>

   <fo:table-cell>
      <fo:block font-size="10pt" >
		<xsl:value-of select = "uptime" /> minutes
		</fo:block>
    </fo:table-cell>
 </fo:table-row>


  <fo:table-row>
    <fo:table-cell>
      <fo:block font-size="10pt" >
		Uptime
		</fo:block>
    </fo:table-cell>

   <fo:table-cell>
      <fo:block font-size="10pt" >
		<xsl:variable name = "ut" select = "uptime div 60" /> <xsl:value-of select = "format-number($ut,'0000000.00')" /> hours
		</fo:block>
    </fo:table-cell>
 </fo:table-row>


  <fo:table-row>
    <fo:table-cell>
      <fo:block font-size="10pt" >
		Uptime
		</fo:block>
    </fo:table-cell>

   <fo:table-cell>
      <fo:block font-size="10pt" >
		<xsl:variable name = "ut2" select = "uptime div 1440" /> <xsl:value-of select = "format-number($ut2,'####.0')" /> days
		</fo:block>
    </fo:table-cell>
 </fo:table-row>


  <fo:table-row>
    <fo:table-cell>
      <fo:block font-size="10pt" >
		CAN S/W Version
		</fo:block>
    </fo:table-cell>

   <fo:table-cell>
      <fo:block font-size="10pt" >
		<xsl:value-of select = "cansw" />
		</fo:block>
    </fo:table-cell>
 </fo:table-row>


  <fo:table-row>
    <fo:table-cell>
      <fo:block font-size="10pt" >
		PSU S/W Version
		</fo:block>
    </fo:table-cell>

   <fo:table-cell>
      <fo:block font-size="10pt" >
		<xsl:value-of select = "psusw" />
		</fo:block>
    </fo:table-cell>
 </fo:table-row>




    
</fo:table-body>
</fo:table>
<fo:block font-size="10pt">--------------------------------------------------------</fo:block>
<fo:block font-size="10pt">Note: If not stated otherwise all units are in A, V, W or s</fo:block>
 
     </fo:table-cell>
   
      <fo:table-cell>
      <fo:block font-size="20pt" font-weight="bold">
	<xsl:if test="@result='pass'">
	PASSED
	</xsl:if>
	<xsl:if test="@result='fail'">
	FAILED
	</xsl:if>
		</fo:block>
    </fo:table-cell>
     
     
 </fo:table-row>
    </fo:table-body>
</fo:table>


<xsl:apply-templates  />



<!--		End of the report template -->
 </xsl:template> 

<!-- 		Other Templates  -->






  <xsl:template match = "group[@name='Settings']">
        <fo:block font-size="14pt"
        font-weight = "bold" 
        padding-after = "0.5cm"
        padding-before = "0.5cm" >
		<xsl:value-of select="desc" /> - <xsl:value-of select="@result" />
		</fo:block>

<xsl:for-each select = "descendant::syserr">
<fo:block font-size="10pt" font-weight = "bold" >
<xsl:value-of select="." /> - <xsl:value-of select="ancestor::chan/@name" />
</fo:block>
</xsl:for-each>

<xsl:for-each select = "descendant::remark">
<fo:block font-size="10pt">
<xsl:value-of select="ancestor::chan/@name" /><xsl:value-of select="." />
</fo:block>
</xsl:for-each>

<fo:block font-size="10pt" font-weight = "bold" padding-after = "0.2cm"> </fo:block>
<xsl:for-each select = "descendant::comment">
<fo:block font-size="10pt" font-weight = "bold" >
Channel <xsl:value-of select="ancestor::chan/@name" />: <xsl:value-of select="." />
</fo:block>
</xsl:for-each>
<fo:block font-size="10pt" font-weight = "bold" padding-after = "0.2cm"> </fo:block>

<xsl:if test = "boolean(opcomm)">

<fo:table>
<fo:table-column  />
<fo:table-column  />
<fo:table-body><fo:table-row>
<fo:table-cell>
       <fo:block font-size="14pt"
       font-weight = "bold" 
        padding-after = "0.5cm"
        padding-before = "0.5cm" >
		Operator Comment
		</fo:block>

</fo:table-cell><fo:table-cell> 

      <fo:block font-size="14pt"
        padding-after = "0.5cm"
        padding-before = "0.5cm" >
		<xsl:value-of select="opcomm" />
		</fo:block>
</fo:table-cell>
</fo:table-row></fo:table-body></fo:table>

</xsl:if>



<fo:table font-size="10pt" table-layout = "fixed" width = "100%">
<fo:table-column />
<fo:table-column />
<fo:table-column column-width = "5cm" />
<fo:table-column />
<fo:table-column />
<fo:table-column />
<fo:table-body>
<fo:table-row>
<fo:table-cell><fo:block>chan</fo:block></fo:table-cell>
<fo:table-cell><fo:block>cname</fo:block></fo:table-cell>
<fo:table-cell><fo:block>Name</fo:block></fo:table-cell>
<fo:table-cell><fo:block>Min</fo:block></fo:table-cell>
<fo:table-cell><fo:block>Value</fo:block></fo:table-cell>
<fo:table-cell><fo:block>Max</fo:block></fo:table-cell>
</fo:table-row>
<xsl:for-each select = "chan">
<xsl:for-each select = "group">
<fo:table-row>
<fo:table-cell><fo:block><xsl:value-of select = "parent::*/@name" /></fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "parent::*/@val" /></fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "@name" /></fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "format-number(Min,'###0.0')" /></fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "format-number(Value,'###0.00')" /></fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "format-number(Max,'###0.00')" /></fo:block></fo:table-cell>
</fo:table-row>
</xsl:for-each>
</xsl:for-each>
</fo:table-body>
</fo:table>


 </xsl:template>



  <xsl:template match = "group[@name='CLimTest']">
        <fo:block font-size="14pt"
        font-weight = "bold" 
        padding-after = "0.5cm"
        padding-before = "0.5cm" >
		<xsl:value-of select="desc" /> - <xsl:value-of select="@result" />
		</fo:block>

<xsl:for-each select = "descendant::syserr">
<fo:block font-size="10pt" font-weight = "bold" >
<xsl:value-of select="." /> - <xsl:value-of select="ancestor::chan/@name" />
</fo:block>
</xsl:for-each>

<xsl:for-each select = "descendant::remark">
<fo:block font-size="10pt">
<xsl:value-of select="ancestor::chan/@name" /><xsl:value-of select="." />
</fo:block>
</xsl:for-each>

<fo:block font-size="10pt" font-weight = "bold" padding-after = "0.2cm"> </fo:block>
<xsl:for-each select = "descendant::comment">
<fo:block font-size="10pt" font-weight = "bold" >
Channel <xsl:value-of select="ancestor::chan/@name" />: <xsl:value-of select="." />
</fo:block>
</xsl:for-each>
<fo:block font-size="10pt" font-weight = "bold" padding-after = "0.2cm"> </fo:block>

<xsl:if test = "boolean(opcomm)">

<fo:table>
<fo:table-column  />
<fo:table-column  />
<fo:table-body><fo:table-row>
<fo:table-cell>
       <fo:block font-size="14pt"
       font-weight = "bold" 
        padding-after = "0.5cm"
        padding-before = "0.5cm" >
		Operator Comment
		</fo:block>

</fo:table-cell><fo:table-cell> 

      <fo:block font-size="14pt"
        padding-after = "0.5cm"
        padding-before = "0.5cm" >
		<xsl:value-of select="opcomm" />
		</fo:block>
</fo:table-cell>
</fo:table-row></fo:table-body></fo:table>

</xsl:if>



<fo:table font-size="10pt" table-layout = "fixed" width = "100%">
<fo:table-column />
<fo:table-column />
<fo:table-column />
<fo:table-column />
<fo:table-column />
<fo:table-column />
<fo:table-body>
<fo:table-row>
<fo:table-cell><fo:block>chan</fo:block></fo:table-cell>
<fo:table-cell><fo:block>cname</fo:block></fo:table-cell>
<fo:table-cell><fo:block>Curr Limit</fo:block></fo:table-cell>
<fo:table-cell><fo:block>tripCurr</fo:block></fo:table-cell>
<fo:table-cell><fo:block>nomCurr</fo:block></fo:table-cell>
<fo:table-cell><fo:block>% from setting</fo:block></fo:table-cell>
</fo:table-row>
<xsl:for-each select = "chan">
<fo:table-row>
<fo:table-cell><fo:block><xsl:value-of select = "@name" /></fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "@val" /></fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "format-number(currlim,'###0.00')" /></fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:choose><xsl:when test="tripcurr/@result = 'fail'">
<fo:inline font-weight="bold"><xsl:value-of select = "format-number(tripcurr,'###0.00')" /> **</fo:inline>
</xsl:when><xsl:otherwise><xsl:value-of select = "format-number(tripcurr,'###0.00')" /></xsl:otherwise></xsl:choose>
</fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "format-number(refcurr,'###0.00')" /></fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:choose><xsl:when test="dpc/@result = 'fail'">
<fo:inline font-weight="bold"><xsl:value-of select = "format-number(dpc,'###0.00')" /> **</fo:inline>
</xsl:when><xsl:otherwise><xsl:value-of select = "format-number(dpc,'###0.00')" /></xsl:otherwise></xsl:choose>
</fo:block></fo:table-cell>
</fo:table-row>
</xsl:for-each>
</fo:table-body>
</fo:table>


 </xsl:template>



  <xsl:template match = "group[@name='SensorTest']">
        <fo:block font-size="14pt"
        font-weight = "bold" 
        padding-after = "0.5cm"
        padding-before = "0.5cm" >
		<xsl:value-of select="desc" /> - <xsl:value-of select="@result" />
		</fo:block>

<xsl:for-each select = "descendant::syserr">
<fo:block font-size="10pt" font-weight = "bold" >
<xsl:value-of select="." /> - <xsl:value-of select="ancestor::chan/@name" />
</fo:block>
</xsl:for-each>

<xsl:for-each select = "descendant::remark">
<fo:block font-size="10pt">
<xsl:value-of select="ancestor::chan/@name" /><xsl:value-of select="." />
</fo:block>
</xsl:for-each>

<fo:block font-size="10pt" font-weight = "bold" padding-after = "0.2cm"> </fo:block>
<xsl:for-each select = "descendant::comment">
<fo:block font-size="10pt" font-weight = "bold" >
Channel <xsl:value-of select="ancestor::chan/@name" />: <xsl:value-of select="." />
</fo:block>
</xsl:for-each>
<fo:block font-size="10pt" font-weight = "bold" padding-after = "0.2cm"> </fo:block>

<xsl:if test = "boolean(opcomm)">

<fo:table>
<fo:table-column  />
<fo:table-column  />
<fo:table-body><fo:table-row>
<fo:table-cell>
       <fo:block font-size="14pt"
       font-weight = "bold" 
        padding-after = "0.5cm"
        padding-before = "0.5cm" >
		Operator Comment
		</fo:block>

</fo:table-cell><fo:table-cell> 

      <fo:block font-size="14pt"
        padding-after = "0.5cm"
        padding-before = "0.5cm" >
		<xsl:value-of select="opcomm" />
		</fo:block>
</fo:table-cell>
</fo:table-row></fo:table-body></fo:table>

</xsl:if>


<fo:block font-size="10pt">V1 = Difference of voltages measured by PSU and DVM with no load</fo:block>
<fo:block font-size="10pt">V2 = Difference of voltages measured by PSU and DVM under nominal load</fo:block>
<fo:block font-size="10pt" padding-after = "0.2cm">C1 = Difference of currents measured by PSU and load under nominal load</fo:block>

<fo:table font-size="10pt" table-layout = "fixed" width = "100%">
<fo:table-column />
<fo:table-column />
<fo:table-column />
<fo:table-column />
<fo:table-column />
<fo:table-column />
<fo:table-body>
<fo:table-row>
<fo:table-cell><fo:block>chan</fo:block></fo:table-cell>
<fo:table-cell><fo:block>cname</fo:block></fo:table-cell>
<fo:table-cell><fo:block>Voltage Set-Psu</fo:block></fo:table-cell>
<fo:table-cell><fo:block>V1</fo:block></fo:table-cell>
<fo:table-cell><fo:block>V2</fo:block></fo:table-cell>
<fo:table-cell><fo:block>C1</fo:block></fo:table-cell>
</fo:table-row>
<xsl:for-each select = "chan">
<fo:table-row>
<fo:table-cell><fo:block><xsl:value-of select = "@name" /></fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "@val" /></fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:choose><xsl:when test="v_accuracy1/@result = 'fail'">
<fo:inline font-weight="bold"><xsl:value-of select = "format-number(v_accuracy1,'###0.00')" /> **</fo:inline>
</xsl:when><xsl:otherwise><xsl:value-of select = "format-number(v_accuracy1,'###0.00')" /></xsl:otherwise></xsl:choose>
</fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:choose><xsl:when test="v_accuracy2/@result = 'fail'">
<fo:inline font-weight="bold"><xsl:value-of select = "format-number(v_accuracy2,'###0.00')" /> **</fo:inline>
</xsl:when><xsl:otherwise><xsl:value-of select = "format-number(v_accuracy2,'###0.00')" /></xsl:otherwise></xsl:choose>
</fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:choose><xsl:when test="volt_accuracy/@result = 'fail'">
<fo:inline font-weight="bold"><xsl:value-of select = "format-number(volt_accuracy,'###0.00')" /> **</fo:inline>
</xsl:when><xsl:otherwise><xsl:value-of select = "format-number(volt_accuracy,'###0.00')" /></xsl:otherwise></xsl:choose>
</fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:choose><xsl:when test="curr_accuracy/@result = 'fail'">
<fo:inline font-weight="bold"><xsl:value-of select = "format-number(curr_accuracy,'###0.00')" /> **</fo:inline>
</xsl:when><xsl:otherwise><xsl:value-of select = "format-number(curr_accuracy,'###0.00')" /></xsl:otherwise></xsl:choose>
</fo:block></fo:table-cell>
</fo:table-row>
</xsl:for-each>
</fo:table-body>
</fo:table>


 </xsl:template>



  <xsl:template match = "group[@name='Mains']">
        <fo:block font-size="14pt"
        font-weight = "bold" 
        padding-after = "0.5cm"
        padding-before = "0.5cm" >
		<xsl:value-of select="desc" /> - <xsl:value-of select="@result" />
		</fo:block>

<xsl:for-each select = "descendant::syserr">
<fo:block font-size="10pt" font-weight = "bold" >
<xsl:value-of select="." /> - <xsl:value-of select="ancestor::chan/@name" />
</fo:block>
</xsl:for-each>

<xsl:for-each select = "descendant::remark">
<fo:block font-size="10pt">
<xsl:value-of select="ancestor::chan/@name" /><xsl:value-of select="." />
</fo:block>
</xsl:for-each>

<fo:block font-size="10pt" font-weight = "bold" padding-after = "0.2cm"> </fo:block>
<xsl:for-each select = "descendant::comment">
<fo:block font-size="10pt" font-weight = "bold" >
Channel <xsl:value-of select="ancestor::chan/@name" />: <xsl:value-of select="." />
</fo:block>
</xsl:for-each>
<fo:block font-size="10pt" font-weight = "bold" padding-after = "0.2cm"> </fo:block>

<xsl:if test = "boolean(opcomm)">

<fo:table>
<fo:table-column  />
<fo:table-column  />
<fo:table-body><fo:table-row>
<fo:table-cell>
       <fo:block font-size="14pt"
       font-weight = "bold" 
        padding-after = "0.5cm"
        padding-before = "0.5cm" >
		Operator Comment
		</fo:block>

</fo:table-cell><fo:table-cell> 

      <fo:block font-size="14pt"
        padding-after = "0.5cm"
        padding-before = "0.5cm" >
		<xsl:value-of select="opcomm" />
		</fo:block>
</fo:table-cell>
</fo:table-row></fo:table-body></fo:table>

</xsl:if>



<fo:table font-size="10pt" table-layout = "fixed" width = "100%">
<fo:table-column column-width = "3cm" />
<fo:table-column />
<fo:table-body>
<fo:table-row>
<fo:table-cell><fo:block></fo:block></fo:table-cell>
<fo:table-cell><fo:block></fo:block></fo:table-cell>
</fo:table-row>
<fo:table-row>
<fo:table-cell><fo:block>Input Power</fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "inp" /></fo:block></fo:table-cell>
</fo:table-row>
<fo:table-row>
<fo:table-cell><fo:block>Output Power</fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "outp" /></fo:block></fo:table-cell>
</fo:table-row>
<fo:table-row>
<fo:table-cell><fo:block>Efficiency (%)</fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "format-number(eff,'###0.0')" /></fo:block></fo:table-cell>
</fo:table-row>
</fo:table-body>
</fo:table>


<fo:table font-size="10pt" table-layout = "fixed" width = "100%">
<fo:table-column />
<fo:table-column />
<fo:table-column />
<fo:table-column />
<fo:table-body>
<fo:table-row>
<fo:table-cell><fo:block>Channel</fo:block></fo:table-cell>
<fo:table-cell><fo:block>Power in</fo:block></fo:table-cell>
<fo:table-cell><fo:block>Power out</fo:block></fo:table-cell>
<fo:table-cell><fo:block>Efficiency</fo:block></fo:table-cell>
</fo:table-row>
<xsl:for-each select = "chan">
<fo:table-row>
<fo:table-cell><fo:block><xsl:value-of select = "@val" /></fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "format-number(inp,'####0.00')" /></fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "format-number(outp,'####0.00')" /></fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "format-number(eff,'####0.00')" /></fo:block></fo:table-cell>
</fo:table-row>
</xsl:for-each>
</fo:table-body>
</fo:table>


 </xsl:template>



  <xsl:template match = "group[@name='StatregTest']">
        <fo:block font-size="14pt"
        font-weight = "bold" 
        padding-after = "0.5cm"
        padding-before = "0.5cm" >
		<xsl:value-of select="desc" /> - <xsl:value-of select="@result" />
		</fo:block>

<xsl:for-each select = "descendant::syserr">
<fo:block font-size="10pt" font-weight = "bold" >
<xsl:value-of select="." /> - <xsl:value-of select="ancestor::chan/@name" />
</fo:block>
</xsl:for-each>

<xsl:for-each select = "descendant::remark">
<fo:block font-size="10pt">
<xsl:value-of select="ancestor::chan/@name" /><xsl:value-of select="." />
</fo:block>
</xsl:for-each>

<fo:block font-size="10pt" font-weight = "bold" padding-after = "0.2cm"> </fo:block>
<xsl:for-each select = "descendant::comment">
<fo:block font-size="10pt" font-weight = "bold" >
Channel <xsl:value-of select="ancestor::chan/@name" />: <xsl:value-of select="." />
</fo:block>
</xsl:for-each>
<fo:block font-size="10pt" font-weight = "bold" padding-after = "0.2cm"> </fo:block>

<xsl:if test = "boolean(opcomm)">

<fo:table>
<fo:table-column  />
<fo:table-column  />
<fo:table-body><fo:table-row>
<fo:table-cell>
       <fo:block font-size="14pt"
       font-weight = "bold" 
        padding-after = "0.5cm"
        padding-before = "0.5cm" >
		Operator Comment
		</fo:block>

</fo:table-cell><fo:table-cell> 

      <fo:block font-size="14pt"
        padding-after = "0.5cm"
        padding-before = "0.5cm" >
		<xsl:value-of select="opcomm" />
		</fo:block>
</fo:table-cell>
</fo:table-row></fo:table-body></fo:table>

</xsl:if>


<fo:block font-size="10pt">V1 = Voltage measured by DVM without load</fo:block>
<fo:block font-size="10pt">V2 = Voltage measured by DVM with nominal load</fo:block>
<fo:block font-size="10pt">Ratio = abs(V1 - V2) / V2</fo:block>
<fo:block font-size="10pt">V3 = Voltage measured by PSU with nominal load</fo:block>
<fo:block font-size="10pt" padding-after = "0.2cm">I1 = Current measured by load with nominal load</fo:block>

<fo:table font-size="10pt" table-layout = "fixed" width = "100%">
<fo:table-column />
<fo:table-column />
<fo:table-column />
<fo:table-column />
<fo:table-column />
<fo:table-column />
<fo:table-column />
<fo:table-column />
<fo:table-body>
<fo:table-row>
<fo:table-cell><fo:block>chan</fo:block></fo:table-cell>
<fo:table-cell><fo:block>cname</fo:block></fo:table-cell>
<fo:table-cell><fo:block>Input V</fo:block></fo:table-cell>
<fo:table-cell><fo:block>V1</fo:block></fo:table-cell>
<fo:table-cell><fo:block>V2</fo:block></fo:table-cell>
<fo:table-cell><fo:block>Ratio</fo:block></fo:table-cell>
<fo:table-cell><fo:block>V3</fo:block></fo:table-cell>
<fo:table-cell><fo:block>I1</fo:block></fo:table-cell>
</fo:table-row>
<xsl:for-each select = "chan">
<xsl:for-each select = "group">
<fo:table-row>
<fo:table-cell><fo:block><xsl:value-of select = "parent::*/@name" /></fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "parent::*/@val" /></fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "@val" /></fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "format-number(v1,'###0.000')" /></fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "format-number(v2,'###0.000')" /></fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:choose><xsl:when test="ratio/@result = 'fail'">
<fo:inline font-weight="bold"><xsl:value-of select = "format-number(ratio,'##0.000')" /> **</fo:inline>
</xsl:when><xsl:otherwise><xsl:value-of select = "format-number(ratio,'##0.000')" /></xsl:otherwise></xsl:choose>
</fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "format-number(vps2,'###0.000')" /></fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "format-number(c2,'###0.000')" /></fo:block></fo:table-cell>
</fo:table-row>
</xsl:for-each>
</xsl:for-each>
</fo:table-body>
</fo:table>


 </xsl:template>



  <xsl:template match = "group[@name='Soak']">
        <fo:block font-size="14pt"
        font-weight = "bold" 
        padding-after = "0.5cm"
        padding-before = "0.5cm" >
		<xsl:value-of select="desc" /> - <xsl:value-of select="@result" />
		</fo:block>

<xsl:for-each select = "descendant::syserr">
<fo:block font-size="10pt" font-weight = "bold" >
<xsl:value-of select="." /> - <xsl:value-of select="ancestor::chan/@name" />
</fo:block>
</xsl:for-each>

<xsl:for-each select = "descendant::remark">
<fo:block font-size="10pt">
<xsl:value-of select="ancestor::chan/@name" /><xsl:value-of select="." />
</fo:block>
</xsl:for-each>

<fo:block font-size="10pt" font-weight = "bold" padding-after = "0.2cm"> </fo:block>
<xsl:for-each select = "descendant::comment">
<fo:block font-size="10pt" font-weight = "bold" >
Channel <xsl:value-of select="ancestor::chan/@name" />: <xsl:value-of select="." />
</fo:block>
</xsl:for-each>
<fo:block font-size="10pt" font-weight = "bold" padding-after = "0.2cm"> </fo:block>

<xsl:if test = "boolean(opcomm)">

<fo:table>
<fo:table-column  />
<fo:table-column  />
<fo:table-body><fo:table-row>
<fo:table-cell>
       <fo:block font-size="14pt"
       font-weight = "bold" 
        padding-after = "0.5cm"
        padding-before = "0.5cm" >
		Operator Comment
		</fo:block>

</fo:table-cell><fo:table-cell> 

      <fo:block font-size="14pt"
        padding-after = "0.5cm"
        padding-before = "0.5cm" >
		<xsl:value-of select="opcomm" />
		</fo:block>
</fo:table-cell>
</fo:table-row></fo:table-body></fo:table>

</xsl:if>



<fo:table font-size="10pt" table-layout = "fixed" width = "100%">
<fo:table-column column-width = "3cm" />
<fo:table-column />
<fo:table-body>
<fo:table-row>
<fo:table-cell><fo:block></fo:block></fo:table-cell>
<fo:table-cell><fo:block></fo:block></fo:table-cell>
</fo:table-row>
<fo:table-row>
<fo:table-cell><fo:block>Store Interval</fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "Time" /></fo:block></fo:table-cell>
</fo:table-row>
</fo:table-body>
</fo:table>

<fo:block font-size="10pt">Vdvm = Voltage measured by DVM</fo:block>
<fo:block font-size="10pt">Vpsu = Voltage measured by PSU</fo:block>
<fo:block font-size="10pt">Iload = Current measured by load</fo:block>
<fo:block font-size="10pt" padding-after = "0.2cm">Ipsu = Current measured by PSU</fo:block>

<fo:table font-size="10pt" table-layout = "fixed" width = "100%">
<fo:table-column />
<fo:table-column />
<fo:table-column />
<fo:table-column />
<fo:table-column />
<fo:table-column />
<fo:table-body>
<fo:table-row>
<fo:table-cell><fo:block>Time</fo:block></fo:table-cell>
<fo:table-cell><fo:block>Channel</fo:block></fo:table-cell>
<fo:table-cell><fo:block>Vpsu</fo:block></fo:table-cell>
<fo:table-cell><fo:block>Ipsu</fo:block></fo:table-cell>
<fo:table-cell><fo:block>Vdvm</fo:block></fo:table-cell>
<fo:table-cell><fo:block>Iload</fo:block></fo:table-cell>
</fo:table-row>
<xsl:for-each select = "chan">
<xsl:for-each select = "group">
<fo:table-row>
<fo:table-cell><fo:block><xsl:value-of select = "parent::*/@val" /></fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "channel" /></fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "format-number(ivolt,'###0.00')" /></fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "format-number(icurr,'###0.00')" /></fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "format-number(evolt,'###0.00')" /></fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "format-number(ecurr,'###0.00')" /></fo:block></fo:table-cell>
</fo:table-row>
</xsl:for-each>
</xsl:for-each>
</fo:table-body>
</fo:table>


 </xsl:template>



  <xsl:template match = "group[@name='Soak2']">
        <fo:block font-size="14pt"
        font-weight = "bold" 
        padding-after = "0.5cm"
        padding-before = "0.5cm" >
		</fo:block>

<xsl:for-each select = "descendant::syserr">
<fo:block font-size="10pt" font-weight = "bold" >
<xsl:value-of select="." /> - <xsl:value-of select="ancestor::chan/@name" />
</fo:block>
</xsl:for-each>

<xsl:for-each select = "descendant::comment">
<fo:block font-size="10pt" font-weight = "bold" >
Channel <xsl:value-of select="ancestor::chan/@name" />: <xsl:value-of select="." />
</fo:block>
</xsl:for-each>

<xsl:for-each select = "descendant::remark">
<fo:block font-size="10pt" font-weight = "bold">
<xsl:value-of select="ancestor::chan/@name" /><xsl:value-of select="." />
</fo:block>
</xsl:for-each>

<xsl:if test = "boolean(opcomm)">

<fo:table>
<fo:table-column  />
<fo:table-column  />
<fo:table-body><fo:table-row>
<fo:table-cell>
       <fo:block font-size="14pt"
       font-weight = "bold" 
        padding-after = "0.5cm"
        padding-before = "0.5cm" >
		Operator Comment
		</fo:block>

</fo:table-cell><fo:table-cell> 

      <fo:block font-size="14pt"
        padding-after = "0.5cm"
        padding-before = "0.5cm" >
		<xsl:value-of select="opcomm" />
		</fo:block>
</fo:table-cell>
</fo:table-row></fo:table-body></fo:table>

</xsl:if>





<fo:block font-size="10pt" padding-after = "0.2cm">Minimum values</fo:block>

<fo:table font-size="10pt" table-layout = "fixed" width = "100%">
<fo:table-column />
<fo:table-column />
<fo:table-column />
<fo:table-column />
<fo:table-column />
<fo:table-body>
<fo:table-row>
<fo:table-cell><fo:block>Channel</fo:block></fo:table-cell>
<fo:table-cell><fo:block>Vpsu</fo:block></fo:table-cell>
<fo:table-cell><fo:block>Ipsu</fo:block></fo:table-cell>
<fo:table-cell><fo:block>Vdvm</fo:block></fo:table-cell>
<fo:table-cell><fo:block>Iload</fo:block></fo:table-cell>
</fo:table-row>
<xsl:for-each select = "chan">
<xsl:for-each select = "group">
<fo:table-row>
<fo:table-cell><fo:block><xsl:value-of select = "channel" /></fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "format-number(ivoltmin,'###0.00')" /></fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "format-number(icurrmin,'###0.00')" /></fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "format-number(evoltmin,'###0.00')" /></fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "format-number(ecurrmin,'###0.00')" /></fo:block></fo:table-cell>
</fo:table-row>
</xsl:for-each>
</xsl:for-each>
</fo:table-body>
</fo:table>

<fo:block font-size="10pt" padding-after = "0.2cm">Maximum values</fo:block>

<fo:table font-size="10pt" table-layout = "fixed" width = "100%">
<fo:table-column />
<fo:table-column />
<fo:table-column />
<fo:table-column />
<fo:table-column />
<fo:table-body>
<fo:table-row>
<fo:table-cell><fo:block>Channel</fo:block></fo:table-cell>
<fo:table-cell><fo:block>Vpsu</fo:block></fo:table-cell>
<fo:table-cell><fo:block>Ipsu</fo:block></fo:table-cell>
<fo:table-cell><fo:block>Vdvm</fo:block></fo:table-cell>
<fo:table-cell><fo:block>Iload</fo:block></fo:table-cell>
</fo:table-row>
<xsl:for-each select = "chan">
<xsl:for-each select = "group">
<fo:table-row>
<fo:table-cell><fo:block><xsl:value-of select = "channel" /></fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "format-number(ivoltmax,'###0.00')" /></fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "format-number(icurrmax,'###0.00')" /></fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "format-number(evoltmax,'###0.00')" /></fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "format-number(ecurrmax,'###0.00')" /></fo:block></fo:table-cell>
</fo:table-row>
</xsl:for-each>
</xsl:for-each>
</fo:table-body>
</fo:table>


 </xsl:template>



  <xsl:template match = "group[@name='OvervoltageTripTest']">
        <fo:block font-size="14pt"
        font-weight = "bold" 
        padding-after = "0.5cm"
        padding-before = "0.5cm" >
		<xsl:value-of select="desc" /> - <xsl:value-of select="@result" />
		</fo:block>

<xsl:for-each select = "descendant::syserr">
<fo:block font-size="10pt" font-weight = "bold" >
<xsl:value-of select="." /> - <xsl:value-of select="ancestor::chan/@name" />
</fo:block>
</xsl:for-each>

<xsl:for-each select = "descendant::remark">
<fo:block font-size="10pt">
<xsl:value-of select="ancestor::chan/@name" /><xsl:value-of select="." />
</fo:block>
</xsl:for-each>

<fo:block font-size="10pt" font-weight = "bold" padding-after = "0.2cm"> </fo:block>
<xsl:for-each select = "descendant::comment">
<fo:block font-size="10pt" font-weight = "bold" >
Channel <xsl:value-of select="ancestor::chan/@name" />: <xsl:value-of select="." />
</fo:block>
</xsl:for-each>
<fo:block font-size="10pt" font-weight = "bold" padding-after = "0.2cm"> </fo:block>

<xsl:if test = "boolean(opcomm)">

<fo:table>
<fo:table-column  />
<fo:table-column  />
<fo:table-body><fo:table-row>
<fo:table-cell>
       <fo:block font-size="14pt"
       font-weight = "bold" 
        padding-after = "0.5cm"
        padding-before = "0.5cm" >
		Operator Comment
		</fo:block>

</fo:table-cell><fo:table-cell> 

      <fo:block font-size="14pt"
        padding-after = "0.5cm"
        padding-before = "0.5cm" >
		<xsl:value-of select="opcomm" />
		</fo:block>
</fo:table-cell>
</fo:table-row></fo:table-body></fo:table>

</xsl:if>



<fo:table font-size="10pt" table-layout = "fixed" width = "100%">
<fo:table-column />
<fo:table-column />
<fo:table-column />
<fo:table-column />
<fo:table-column />
<fo:table-body>
<fo:table-row>
<fo:table-cell><fo:block>chan</fo:block></fo:table-cell>
<fo:table-cell><fo:block>cname</fo:block></fo:table-cell>
<fo:table-cell><fo:block>Set OV trip</fo:block></fo:table-cell>
<fo:table-cell><fo:block>Trip V</fo:block></fo:table-cell>
<fo:table-cell><fo:block>% from setting</fo:block></fo:table-cell>
</fo:table-row>
<xsl:for-each select = "chan">
<fo:table-row>
<fo:table-cell><fo:block><xsl:value-of select = "@name" /></fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "@val" /></fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "format-number(ovcompare,'###0.000')" /></fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:choose><xsl:when test="rvolt/@result = 'fail'">
<fo:inline font-weight="bold"><xsl:value-of select = "format-number(rvolt,'###0.000')" /> **</fo:inline>
</xsl:when><xsl:otherwise><xsl:value-of select = "format-number(rvolt,'###0.000')" /></xsl:otherwise></xsl:choose>
</fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:choose><xsl:when test="dpc/@result = 'fail'">
<fo:inline font-weight="bold"><xsl:value-of select = "format-number(dpc,'###0.00')" /> **</fo:inline>
</xsl:when><xsl:otherwise><xsl:value-of select = "format-number(dpc,'###0.00')" /></xsl:otherwise></xsl:choose>
</fo:block></fo:table-cell>
</fo:table-row>
</xsl:for-each>
</fo:table-body>
</fo:table>


 </xsl:template>



  <xsl:template match = "group[@name='OvervoltageTest']">
        <fo:block font-size="14pt"
        font-weight = "bold" 
        padding-after = "0.5cm"
        padding-before = "0.5cm" >
		<xsl:value-of select="desc" /> - <xsl:value-of select="@result" />
		</fo:block>

<xsl:for-each select = "descendant::syserr">
<fo:block font-size="10pt" font-weight = "bold" >
<xsl:value-of select="." /> - <xsl:value-of select="ancestor::chan/@name" />
</fo:block>
</xsl:for-each>

<xsl:for-each select = "descendant::remark">
<fo:block font-size="10pt">
<xsl:value-of select="ancestor::chan/@name" /><xsl:value-of select="." />
</fo:block>
</xsl:for-each>

<fo:block font-size="10pt" font-weight = "bold" padding-after = "0.2cm"> </fo:block>
<xsl:for-each select = "descendant::comment">
<fo:block font-size="10pt" font-weight = "bold" >
Channel <xsl:value-of select="ancestor::chan/@name" />: <xsl:value-of select="." />
</fo:block>
</xsl:for-each>
<fo:block font-size="10pt" font-weight = "bold" padding-after = "0.2cm"> </fo:block>

<xsl:if test = "boolean(opcomm)">

<fo:table>
<fo:table-column  />
<fo:table-column  />
<fo:table-body><fo:table-row>
<fo:table-cell>
       <fo:block font-size="14pt"
       font-weight = "bold" 
        padding-after = "0.5cm"
        padding-before = "0.5cm" >
		Operator Comment
		</fo:block>

</fo:table-cell><fo:table-cell> 

      <fo:block font-size="14pt"
        padding-after = "0.5cm"
        padding-before = "0.5cm" >
		<xsl:value-of select="opcomm" />
		</fo:block>
</fo:table-cell>
</fo:table-row></fo:table-body></fo:table>

</xsl:if>



<fo:table font-size="10pt" table-layout = "fixed" width = "100%">
<fo:table-column />
<fo:table-column />
<fo:table-column />
<fo:table-column />
<fo:table-column />
<fo:table-body>
<fo:table-row>
<fo:table-cell><fo:block>chan</fo:block></fo:table-cell>
<fo:table-cell><fo:block>cname</fo:block></fo:table-cell>
<fo:table-cell><fo:block>Set OV trip</fo:block></fo:table-cell>
<fo:table-cell><fo:block>Trip V</fo:block></fo:table-cell>
<fo:table-cell><fo:block>% from setting</fo:block></fo:table-cell>
</fo:table-row>
<xsl:for-each select = "chan">
<fo:table-row>
<fo:table-cell><fo:block><xsl:value-of select = "@name" /></fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "@val" /></fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "format-number(ovp,'###0.000')" /></fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:choose><xsl:when test="rvolt/@result = 'fail'">
<fo:inline font-weight="bold"><xsl:value-of select = "format-number(rvolt,'###0.000')" /> **</fo:inline>
</xsl:when><xsl:otherwise><xsl:value-of select = "format-number(rvolt,'###0.000')" /></xsl:otherwise></xsl:choose>
</fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:choose><xsl:when test="dpc/@result = 'fail'">
<fo:inline font-weight="bold"><xsl:value-of select = "format-number(dpc,'###0.00')" /> **</fo:inline>
</xsl:when><xsl:otherwise><xsl:value-of select = "format-number(dpc,'###0.00')" /></xsl:otherwise></xsl:choose>
</fo:block></fo:table-cell>
</fo:table-row>
</xsl:for-each>
</fo:table-body>
</fo:table>


 </xsl:template>



  <xsl:template match = "group[@name='RippleTest']">
        <fo:block font-size="14pt"
        font-weight = "bold" 
        padding-after = "0.5cm"
        padding-before = "0.5cm" >
		<xsl:value-of select="desc" /> - <xsl:value-of select="@result" />
		</fo:block>

<xsl:for-each select = "descendant::syserr">
<fo:block font-size="10pt" font-weight = "bold" >
<xsl:value-of select="." /> - <xsl:value-of select="ancestor::chan/@name" />
</fo:block>
</xsl:for-each>

<xsl:for-each select = "descendant::remark">
<fo:block font-size="10pt">
<xsl:value-of select="ancestor::chan/@name" /><xsl:value-of select="." />
</fo:block>
</xsl:for-each>

<fo:block font-size="10pt" font-weight = "bold" padding-after = "0.2cm"> </fo:block>
<xsl:for-each select = "descendant::comment">
<fo:block font-size="10pt" font-weight = "bold" >
Channel <xsl:value-of select="ancestor::chan/@name" />: <xsl:value-of select="." />
</fo:block>
</xsl:for-each>
<fo:block font-size="10pt" font-weight = "bold" padding-after = "0.2cm"> </fo:block>

<xsl:if test = "boolean(opcomm)">

<fo:table>
<fo:table-column  />
<fo:table-column  />
<fo:table-body><fo:table-row>
<fo:table-cell>
       <fo:block font-size="14pt"
       font-weight = "bold" 
        padding-after = "0.5cm"
        padding-before = "0.5cm" >
		Operator Comment
		</fo:block>

</fo:table-cell><fo:table-cell> 

      <fo:block font-size="14pt"
        padding-after = "0.5cm"
        padding-before = "0.5cm" >
		<xsl:value-of select="opcomm" />
		</fo:block>
</fo:table-cell>
</fo:table-row></fo:table-body></fo:table>

</xsl:if>


<fo:block font-size="10pt">pkpk_1 = Ripple in mV measured without load</fo:block>
<fo:block font-size="10pt">pkpk_2 = Ripple in mV measured with 50% load</fo:block>
<fo:block font-size="10pt" padding-after = "0.2cm">pkpk_3 = Ripple in mV measured with 100% load</fo:block>

<fo:table font-size="10pt" table-layout = "fixed" width = "100%">
<fo:table-column />
<fo:table-column />
<fo:table-column />
<fo:table-column />
<fo:table-column />
<fo:table-body>
<fo:table-row>
<fo:table-cell><fo:block>chan</fo:block></fo:table-cell>
<fo:table-cell><fo:block>cname</fo:block></fo:table-cell>
<fo:table-cell><fo:block>pkpk_1</fo:block></fo:table-cell>
<fo:table-cell><fo:block>pkpk_2</fo:block></fo:table-cell>
<fo:table-cell><fo:block>pkpk_3</fo:block></fo:table-cell>
</fo:table-row>
<xsl:for-each select = "chan">
<fo:table-row>
<fo:table-cell><fo:block><xsl:value-of select = "@name" /></fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "@val" /></fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:choose><xsl:when test="pkpk_1/@result = 'fail'">
<fo:inline font-weight="bold"><xsl:value-of select = "format-number(pkpk_1,'###0.000')" /> **</fo:inline>
</xsl:when><xsl:otherwise><xsl:value-of select = "format-number(pkpk_1,'###0.000')" /></xsl:otherwise></xsl:choose>
</fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:choose><xsl:when test="pkpk_2/@result = 'fail'">
<fo:inline font-weight="bold"><xsl:value-of select = "format-number(pkpk_2,'###0.000')" /> **</fo:inline>
</xsl:when><xsl:otherwise><xsl:value-of select = "format-number(pkpk_2,'###0.000')" /></xsl:otherwise></xsl:choose>
</fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:choose><xsl:when test="pkpk_3/@result = 'fail'">
<fo:inline font-weight="bold"><xsl:value-of select = "format-number(pkpk_3,'###0.00')" /> **</fo:inline>
</xsl:when><xsl:otherwise><xsl:value-of select = "format-number(pkpk_3,'###0.00')" /></xsl:otherwise></xsl:choose>
</fo:block></fo:table-cell>
</fo:table-row>
</xsl:for-each>
</fo:table-body>
</fo:table>


 </xsl:template>



  <xsl:template match = "group[@name='CommonMode']">
        <fo:block font-size="14pt"
        font-weight = "bold" 
        padding-after = "0.5cm"
        padding-before = "0.5cm" >
		<xsl:value-of select="desc" /> - <xsl:value-of select="@result" />
		</fo:block>

<xsl:for-each select = "descendant::syserr">
<fo:block font-size="10pt" font-weight = "bold" >
<xsl:value-of select="." /> - <xsl:value-of select="ancestor::chan/@name" />
</fo:block>
</xsl:for-each>

<xsl:for-each select = "descendant::remark">
<fo:block font-size="10pt">
<xsl:value-of select="ancestor::chan/@name" /><xsl:value-of select="." />
</fo:block>
</xsl:for-each>

<fo:block font-size="10pt" font-weight = "bold" padding-after = "0.2cm"> </fo:block>
<xsl:for-each select = "descendant::comment">
<fo:block font-size="10pt" font-weight = "bold" >
Channel <xsl:value-of select="ancestor::chan/@name" />: <xsl:value-of select="." />
</fo:block>
</xsl:for-each>
<fo:block font-size="10pt" font-weight = "bold" padding-after = "0.2cm"> </fo:block>

<xsl:if test = "boolean(opcomm)">

<fo:table>
<fo:table-column  />
<fo:table-column  />
<fo:table-body><fo:table-row>
<fo:table-cell>
       <fo:block font-size="14pt"
       font-weight = "bold" 
        padding-after = "0.5cm"
        padding-before = "0.5cm" >
		Operator Comment
		</fo:block>

</fo:table-cell><fo:table-cell> 

      <fo:block font-size="14pt"
        padding-after = "0.5cm"
        padding-before = "0.5cm" >
		<xsl:value-of select="opcomm" />
		</fo:block>
</fo:table-cell>
</fo:table-row></fo:table-body></fo:table>

</xsl:if>



<fo:table font-size="10pt" table-layout = "fixed" width = "100%">
<fo:table-column column-width = "5cm" />
<fo:table-column />
<fo:table-body>
<fo:table-row>
<fo:table-cell><fo:block></fo:block></fo:table-cell>
<fo:table-cell><fo:block></fo:block></fo:table-cell>
</fo:table-row>
<fo:table-row>
<fo:table-cell><fo:block>pkpk_no_curr (mV)</fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "format-number(pkpk_no_curr,'###0.0')" /></fo:block></fo:table-cell>
</fo:table-row>
<fo:table-row>
<fo:table-cell><fo:block>pkpk_half_curr (mV)</fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "format-number(pkpk_half_curr,'###0.0')" /></fo:block></fo:table-cell>
</fo:table-row>
<fo:table-row>
<fo:table-cell><fo:block>pkpk_full_curr (mV)</fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "format-number(pkpk_full_curr,'###0.0')" /></fo:block></fo:table-cell>
</fo:table-row>
</fo:table-body>
</fo:table>


 </xsl:template>



 
</xsl:stylesheet>




