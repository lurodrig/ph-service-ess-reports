<?xml version="1.0"?> 








 
 
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
	xmlns:fo="http://www.w3.org/1999/XSL/Format">
  <xsl:output method="xml" indent="no"/>

<xsl:decimal-format  NaN="---" />
 
 
 <xsl:template match="/" >

<fo:root > 


  <fo:layout-master-set>
  <!-- fo:layout-master-set defines in its children the page layout: 
       the pagination and layout specifications
      - page-masters: have the role of describing the intended subdivisions 
                       of a page and the geometry of these subdivisions 
                      In this case there is only a simple-page-master which defines the 
                      layout for all pages of the text
  -->
    <!-- layout information -->
    <fo:simple-page-master master-name="A4-first"
                  page-height="29.7cm" 
                  page-width="21cm"
                  margin-top="1cm" 
                  margin-bottom="2cm" 
                  margin-left="2.5cm" 
                  margin-right="2.5cm">
      <fo:region-body margin-top="3cm" margin-bottom="1.5cm" />
      <fo:region-before extent="3cm" region-name="first-header" />
      <fo:region-after extent="1.5cm"/>
    </fo:simple-page-master>
    <fo:simple-page-master master-name="A4-rest"
                  page-height="29.7cm" 
                  page-width="21cm"
                  margin-top="1cm" 
                  margin-bottom="2cm" 
                  margin-left="2.5cm" 
                  margin-right="2.5cm">
      <fo:region-body margin-top="3cm" margin-bottom="1.5cm"/>
      <fo:region-before extent="3cm" region-name="rest-header"/>
      <fo:region-after extent="1.5cm" region-name="rest-footer"/>
    </fo:simple-page-master>
    
    <fo:page-sequence-master master-name="my-sequence" >
    		<fo:single-page-master-reference master-reference="A4-first" />
    		<fo:repeatable-page-master-reference master-reference="A4-rest" />
    	</fo:page-sequence-master>
    
  </fo:layout-master-set>
  <!-- end: defines page layout -->
  
    <fo:page-sequence master-reference="my-sequence">
 
    <fo:static-content flow-name="first-header" font-family = "Times">
      <fo:block>
    <fo:external-graphic src="url('CERNLetterHead.gif')" content-width="18cm" />
  </fo:block>
 </fo:static-content>   
    
    <fo:static-content flow-name="rest-header" font-family = "Times">
  <fo:block font-size="18pt"
  		text-align="center"
  		font-weight="bold">
  		Test of <xsl:value-of select = "//mftype" /> - <xsl:value-of select = "//lhcid" /> performed on <xsl:value-of select = "//date" /> at <xsl:value-of select = "//time" />


  </fo:block>
 </fo:static-content>   
    
    <fo:static-content flow-name="rest-footer" font-family = "Times">
  <fo:block font-size="12pt"
  		text-align="center">
		<fo:page-number />
  </fo:block>
 </fo:static-content>   
    
    
   <fo:flow flow-name="xsl-region-body" font-size="12pt" 
            font-family="Times">
   
 	<xsl:apply-templates />
    </fo:flow> <!-- closes the flow element-->
  </fo:page-sequence> <!-- closes the page-sequence -->
</fo:root>
 </xsl:template>

 <xsl:template match="report">
 
 <!-- 	This template matches the root report
 		it must put out the header and identification information
 		and call the lower templates

		HEADERS
   -->
   
   <fo:block font-size="18pt"
  		text-align="center"
  		font-weight="bold">
  		PH/ESE group: Report for a WIENER RCM

  </fo:block>
 
  <fo:block font-size="14pt"
  		text-align="center"
  		font-weight="bold">
  		

  </fo:block>

<!--		Identification area -->

<fo:table table-layout="fixed" width = "100%" padding-before = "1cm">
<fo:table-column column-width = "12cm" />
<fo:table-column column-width = "3cm" />

<fo:table-body>

  <fo:table-row>
    <fo:table-cell>
 
 
   
 <fo:table table-layout="fixed" width = "100%" >
<fo:table-column  />
<fo:table-column  />

<fo:table-body>





  <fo:table-row>
    <fo:table-cell>
      <fo:block font-size="10pt" >
		Manufacturer's Serial Number:
		</fo:block>
    </fo:table-cell>

   <fo:table-cell>
      <fo:block font-size="10pt" >
		<xsl:value-of select = "mfserial" />
		</fo:block>
    </fo:table-cell>
 </fo:table-row>


  <fo:table-row>
    <fo:table-cell>
      <fo:block font-size="10pt" >
		Manufacturer's Part Number:
		</fo:block>
    </fo:table-cell>

   <fo:table-cell>
      <fo:block font-size="10pt" >
		<xsl:value-of select = "mftype" />
		</fo:block>
    </fo:table-cell>
 </fo:table-row>


  <fo:table-row>
    <fo:table-cell>
      <fo:block font-size="10pt" >
		Technical DB Serial Number:
		</fo:block>
    </fo:table-cell>

   <fo:table-cell>
      <fo:block font-size="10pt" >
		<xsl:value-of select = "lhcid" />
		</fo:block>
    </fo:table-cell>
 </fo:table-row>


  <fo:table-row>
    <fo:table-cell>
      <fo:block font-size="10pt" >
		Operator:
		</fo:block>
    </fo:table-cell>

   <fo:table-cell>
      <fo:block font-size="10pt" >
		<xsl:value-of select = "operator" />
		</fo:block>
    </fo:table-cell>
 </fo:table-row>


  <fo:table-row>
    <fo:table-cell>
      <fo:block font-size="10pt" >
		Date:
		</fo:block>
    </fo:table-cell>

   <fo:table-cell>
      <fo:block font-size="10pt" >
		<xsl:value-of select = "date" /> - <xsl:value-of select = "time" />
		</fo:block>
    </fo:table-cell>
 </fo:table-row>


  <fo:table-row>
    <fo:table-cell>
      <fo:block font-size="10pt" >
		Sensor test:
		</fo:block>
    </fo:table-cell>

   <fo:table-cell>
      <fo:block font-size="10pt" >
		<xsl:value-of select = "sen" />
		</fo:block>
    </fo:table-cell>
 </fo:table-row>


  <fo:table-row>
    <fo:table-cell>
      <fo:block font-size="10pt" >
		Current limit test:
		</fo:block>
    </fo:table-cell>

   <fo:table-cell>
      <fo:block font-size="10pt" >
		<xsl:value-of select = "clim" />
		</fo:block>
    </fo:table-cell>
 </fo:table-row>


  <fo:table-row>
    <fo:table-cell>
      <fo:block font-size="10pt" >
		Overvoltage Trip test:
		</fo:block>
    </fo:table-cell>

   <fo:table-cell>
      <fo:block font-size="10pt" >
		<xsl:value-of select = "ovt" />
		</fo:block>
    </fo:table-cell>
 </fo:table-row>



    
</fo:table-body>
</fo:table>
<fo:block font-size="10pt">--------------------------------------------------------</fo:block>
<fo:block font-size="10pt">Note: If not stated otherwise all units are in A, V, W or s</fo:block>
 
     </fo:table-cell>
   
      <fo:table-cell>
      <fo:block font-size="20pt" font-weight="bold">
	<xsl:if test="@result='pass'">
	PASSED
	</xsl:if>
	<xsl:if test="@result='fail'">
	FAILED
	</xsl:if>
		</fo:block>
    </fo:table-cell>
     
     
 </fo:table-row>
    </fo:table-body>
</fo:table>


<xsl:apply-templates  />



<!--		End of the report template -->
 </xsl:template> 

<!-- 		Other Templates  -->






  <xsl:template match = "group[@name='SensorTest']">
        <fo:block font-size="14pt"
        font-weight = "bold" 
        padding-after = "0.5cm"
        padding-before = "0.5cm" >
		<xsl:value-of select="desc" /> - <xsl:value-of select="@result" />
		</fo:block>

<xsl:for-each select = "descendant::syserr">
<fo:block font-size="10pt" font-weight = "bold" >
<xsl:value-of select="." /> - <xsl:value-of select="ancestor::chan/@name" />
</fo:block>
</xsl:for-each>

<xsl:for-each select = "descendant::remark">
<fo:block font-size="10pt">
<xsl:value-of select="ancestor::chan/@name" /><xsl:value-of select="." />
</fo:block>
</xsl:for-each>

<fo:block font-size="10pt" font-weight = "bold" padding-after = "0.2cm"> </fo:block>
<xsl:for-each select = "descendant::comment">
<fo:block font-size="10pt" font-weight = "bold" >
Channel <xsl:value-of select="ancestor::chan/@name" />: <xsl:value-of select="." />
</fo:block>
</xsl:for-each>
<fo:block font-size="10pt" font-weight = "bold" padding-after = "0.2cm"> </fo:block>

<xsl:if test = "boolean(opcomm)">

<fo:table>
<fo:table-column  />
<fo:table-column  />
<fo:table-body><fo:table-row>
<fo:table-cell>
       <fo:block font-size="14pt"
       font-weight = "bold" 
        padding-after = "0.5cm"
        padding-before = "0.5cm" >
		Operator Comment
		</fo:block>

</fo:table-cell><fo:table-cell> 

      <fo:block font-size="14pt"
        padding-after = "0.5cm"
        padding-before = "0.5cm" >
		<xsl:value-of select="opcomm" />
		</fo:block>
</fo:table-cell>
</fo:table-row></fo:table-body></fo:table>

</xsl:if>


<fo:block font-size="10pt">dV1 = abs(Vpsu - Vset) measured without load</fo:block>
<fo:block font-size="10pt">dV2 = abs(Vpsu - Vdvm) measured without load</fo:block>
<fo:block font-size="10pt">dI1 = abs(Ipsu - Iload) measured without load</fo:block>
<fo:block font-size="10pt">dV3 = abs(Vpsu - Vdvm) measured with load</fo:block>
<fo:block font-size="10pt" padding-after="0.2cm">dI2 = abs(Ipsu - Iload) measured with load</fo:block>

<fo:table font-size="10pt" table-layout = "fixed" width = "100%">
<fo:table-column column-width = "1.5cm" />
<fo:table-column />
<fo:table-column />
<fo:table-column />
<fo:table-column />
<fo:table-column />
<fo:table-body>
<fo:table-row>
<fo:table-cell><fo:block>chan</fo:block></fo:table-cell>
<fo:table-cell><fo:block>dV1</fo:block></fo:table-cell>
<fo:table-cell><fo:block>dV2</fo:block></fo:table-cell>
<fo:table-cell><fo:block>dI1</fo:block></fo:table-cell>
<fo:table-cell><fo:block>dV3</fo:block></fo:table-cell>
<fo:table-cell><fo:block>dI2</fo:block></fo:table-cell>
</fo:table-row>
<xsl:for-each select = "chan">
<fo:table-row>
<fo:table-cell><fo:block><xsl:value-of select = "@name" /></fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:choose><xsl:when test="delta1/@result = 'fail'">
<fo:inline font-weight="bold"><xsl:value-of select = "format-number(delta1,'###0.00')" /> **</fo:inline>
</xsl:when><xsl:otherwise><xsl:value-of select = "format-number(delta1,'###0.00')" /></xsl:otherwise></xsl:choose>
</fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:choose><xsl:when test="delta2/@result = 'fail'">
<fo:inline font-weight="bold"><xsl:value-of select = "format-number(delta2,'###0.00')" /> **</fo:inline>
</xsl:when><xsl:otherwise><xsl:value-of select = "format-number(delta2,'###0.00')" /></xsl:otherwise></xsl:choose>
</fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:choose><xsl:when test="delta3/@result = 'fail'">
<fo:inline font-weight="bold"><xsl:value-of select = "format-number(delta3,'###0.00')" /> **</fo:inline>
</xsl:when><xsl:otherwise><xsl:value-of select = "format-number(delta3,'###0.00')" /></xsl:otherwise></xsl:choose>
</fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:choose><xsl:when test="delta4/@result = 'fail'">
<fo:inline font-weight="bold"><xsl:value-of select = "format-number(delta4,'###0.00')" /> **</fo:inline>
</xsl:when><xsl:otherwise><xsl:value-of select = "format-number(delta4,'###0.00')" /></xsl:otherwise></xsl:choose>
</fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:choose><xsl:when test="delta5/@result = 'fail'">
<fo:inline font-weight="bold"><xsl:value-of select = "format-number(delta5,'###0.00')" /> **</fo:inline>
</xsl:when><xsl:otherwise><xsl:value-of select = "format-number(delta5,'###0.00')" /></xsl:otherwise></xsl:choose>
</fo:block></fo:table-cell>
</fo:table-row>
</xsl:for-each>
</fo:table-body>
</fo:table>


 </xsl:template>



  <xsl:template match = "group[@name='CLimTest']">
        <fo:block font-size="14pt"
        font-weight = "bold" 
        padding-after = "0.5cm"
        padding-before = "0.5cm" >
		<xsl:value-of select="desc" /> - <xsl:value-of select="@result" />
		</fo:block>

<xsl:for-each select = "descendant::syserr">
<fo:block font-size="10pt" font-weight = "bold" >
<xsl:value-of select="." /> - <xsl:value-of select="ancestor::chan/@name" />
</fo:block>
</xsl:for-each>

<xsl:for-each select = "descendant::remark">
<fo:block font-size="10pt">
<xsl:value-of select="ancestor::chan/@name" /><xsl:value-of select="." />
</fo:block>
</xsl:for-each>

<fo:block font-size="10pt" font-weight = "bold" padding-after = "0.2cm"> </fo:block>
<xsl:for-each select = "descendant::comment">
<fo:block font-size="10pt" font-weight = "bold" >
Channel <xsl:value-of select="ancestor::chan/@name" />: <xsl:value-of select="." />
</fo:block>
</xsl:for-each>
<fo:block font-size="10pt" font-weight = "bold" padding-after = "0.2cm"> </fo:block>

<xsl:if test = "boolean(opcomm)">

<fo:table>
<fo:table-column  />
<fo:table-column  />
<fo:table-body><fo:table-row>
<fo:table-cell>
       <fo:block font-size="14pt"
       font-weight = "bold" 
        padding-after = "0.5cm"
        padding-before = "0.5cm" >
		Operator Comment
		</fo:block>

</fo:table-cell><fo:table-cell> 

      <fo:block font-size="14pt"
        padding-after = "0.5cm"
        padding-before = "0.5cm" >
		<xsl:value-of select="opcomm" />
		</fo:block>
</fo:table-cell>
</fo:table-row></fo:table-body></fo:table>

</xsl:if>



<fo:table font-size="10pt" table-layout = "fixed" width = "100%">
<fo:table-column column-width = "1.5cm" />
<fo:table-column />
<fo:table-body>
<fo:table-row>
<fo:table-cell><fo:block>chan</fo:block></fo:table-cell>
<fo:table-cell><fo:block>Trip current</fo:block></fo:table-cell>
</fo:table-row>
<xsl:for-each select = "chan">
<fo:table-row>
<fo:table-cell><fo:block><xsl:value-of select = "@name" /></fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:choose><xsl:when test="tripcurr2/@result = 'fail'">
<fo:inline font-weight="bold"><xsl:value-of select = "format-number(tripcurr2,'###0.00')" /> **</fo:inline>
</xsl:when><xsl:otherwise><xsl:value-of select = "format-number(tripcurr2,'###0.00')" /></xsl:otherwise></xsl:choose>
</fo:block></fo:table-cell>
</fo:table-row>
</xsl:for-each>
</fo:table-body>
</fo:table>


 </xsl:template>



  <xsl:template match = "group[@name='OvervoltageTripTest']">
        <fo:block font-size="14pt"
        font-weight = "bold" 
        padding-after = "0.5cm"
        padding-before = "0.5cm" >
		<xsl:value-of select="desc" /> - <xsl:value-of select="@result" />
		</fo:block>

<xsl:for-each select = "descendant::syserr">
<fo:block font-size="10pt" font-weight = "bold" >
<xsl:value-of select="." /> - <xsl:value-of select="ancestor::chan/@name" />
</fo:block>
</xsl:for-each>

<xsl:for-each select = "descendant::remark">
<fo:block font-size="10pt">
<xsl:value-of select="ancestor::chan/@name" /><xsl:value-of select="." />
</fo:block>
</xsl:for-each>

<fo:block font-size="10pt" font-weight = "bold" padding-after = "0.2cm"> </fo:block>
<xsl:for-each select = "descendant::comment">
<fo:block font-size="10pt" font-weight = "bold" >
Channel <xsl:value-of select="ancestor::chan/@name" />: <xsl:value-of select="." />
</fo:block>
</xsl:for-each>
<fo:block font-size="10pt" font-weight = "bold" padding-after = "0.2cm"> </fo:block>

<xsl:if test = "boolean(opcomm)">

<fo:table>
<fo:table-column  />
<fo:table-column  />
<fo:table-body><fo:table-row>
<fo:table-cell>
       <fo:block font-size="14pt"
       font-weight = "bold" 
        padding-after = "0.5cm"
        padding-before = "0.5cm" >
		Operator Comment
		</fo:block>

</fo:table-cell><fo:table-cell> 

      <fo:block font-size="14pt"
        padding-after = "0.5cm"
        padding-before = "0.5cm" >
		<xsl:value-of select="opcomm" />
		</fo:block>
</fo:table-cell>
</fo:table-row></fo:table-body></fo:table>

</xsl:if>



<fo:table font-size="10pt" table-layout = "fixed" width = "100%">
<fo:table-column column-width = "1.5cm" />
<fo:table-column />
<fo:table-column />
<fo:table-body>
<fo:table-row>
<fo:table-cell><fo:block>chan</fo:block></fo:table-cell>
<fo:table-cell><fo:block>Trip voltage 1</fo:block></fo:table-cell>
<fo:table-cell><fo:block></fo:block></fo:table-cell>
</fo:table-row>
<xsl:for-each select = "chan">
<fo:table-row>
<fo:table-cell><fo:block><xsl:value-of select = "@name" /></fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:choose><xsl:when test="trip_volt1/@result = 'fail'">
<fo:inline font-weight="bold"><xsl:value-of select = "format-number(trip_volt1,'###0.000')" /> **</fo:inline>
</xsl:when><xsl:otherwise><xsl:value-of select = "format-number(trip_volt1,'###0.000')" /></xsl:otherwise></xsl:choose>
</fo:block></fo:table-cell>
</fo:table-row>
</xsl:for-each>
</fo:table-body>
</fo:table>


 </xsl:template>



 
</xsl:stylesheet>


