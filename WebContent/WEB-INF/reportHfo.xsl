<?xml version="1.0"?> 








 
 
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
	xmlns:fo="http://www.w3.org/1999/XSL/Format">
  <xsl:output method="xml" indent="no"/>

<xsl:decimal-format  NaN="---" />
 
 
 <xsl:template match="/" >

<fo:root > 


  <fo:layout-master-set>
  <!-- fo:layout-master-set defines in its children the page layout: 
       the pagination and layout specifications
      - page-masters: have the role of describing the intended subdivisions 
                       of a page and the geometry of these subdivisions 
                      In this case there is only a simple-page-master which defines the 
                      layout for all pages of the text
  -->
    <!-- layout information -->
    <fo:simple-page-master master-name="A4-first"
                  page-height="29.7cm" 
                  page-width="21cm"
                  margin-top="1cm" 
                  margin-bottom="2cm" 
                  margin-left="2.5cm" 
                  margin-right="2.5cm">
      <fo:region-body margin-top="3cm" margin-bottom="1.5cm" />
      <fo:region-before extent="3cm" region-name="first-header" />
      <fo:region-after extent="1.5cm"/>
    </fo:simple-page-master>
    <fo:simple-page-master master-name="A4-rest"
                  page-height="29.7cm" 
                  page-width="21cm"
                  margin-top="1cm" 
                  margin-bottom="2cm" 
                  margin-left="2.5cm" 
                  margin-right="2.5cm">
      <fo:region-body margin-top="3cm" margin-bottom="1.5cm"/>
      <fo:region-before extent="3cm" region-name="rest-header"/>
      <fo:region-after extent="1.5cm" region-name="rest-footer"/>
    </fo:simple-page-master>
    
    <fo:page-sequence-master master-name="my-sequence" >
    		<fo:single-page-master-reference master-reference="A4-first" />
    		<fo:repeatable-page-master-reference master-reference="A4-rest" />
    	</fo:page-sequence-master>
    
  </fo:layout-master-set>
  <!-- end: defines page layout -->
  
    <fo:page-sequence master-reference="my-sequence">
 
    <fo:static-content flow-name="first-header" font-family = "Times">
      <fo:block>
    <fo:external-graphic src="url('CERNLetterHead.gif')" content-width="18cm" />
  </fo:block>
 </fo:static-content>   
    
    <fo:static-content flow-name="rest-header" font-family = "Times">
  <fo:block font-size="18pt"
  		text-align="center"
  		font-weight="bold">
  		Test of <xsl:value-of select = "//mftype" /> - <xsl:value-of select = "//lhcid" /> performed on <xsl:value-of select = "//date" /> at <xsl:value-of select = "//time" />


  </fo:block>
 </fo:static-content>   
    
    <fo:static-content flow-name="rest-footer" font-family = "Times">
  <fo:block font-size="12pt"
  		text-align="center">
		<fo:page-number />
  </fo:block>
 </fo:static-content>   
    
    
   <fo:flow flow-name="xsl-region-body" font-size="12pt" 
            font-family="Times">
   
 	<xsl:apply-templates />
    </fo:flow> <!-- closes the flow element-->
  </fo:page-sequence> <!-- closes the page-sequence -->
</fo:root>
 </xsl:template>

 <xsl:template match="report">
 
 <!-- 	This template matches the root report
 		it must put out the header and identification information
 		and call the lower templates

		HEADERS
   -->
   
   <fo:block font-size="18pt"
  		text-align="center"
  		font-weight="bold">
  		PH/ESE group: Report for a WIENER PFC

  </fo:block>
 
  <fo:block font-size="14pt"
  		text-align="center"
  		font-weight="bold">
  		

  </fo:block>

<!--		Identification area -->

<fo:table table-layout="fixed" width = "100%" padding-before = "1cm">
<fo:table-column column-width = "12cm" />
<fo:table-column column-width = "3cm" />

<fo:table-body>

  <fo:table-row>
    <fo:table-cell>
 
 
   
 <fo:table table-layout="fixed" width = "100%" >
<fo:table-column  />
<fo:table-column  />

<fo:table-body>





  <fo:table-row>
    <fo:table-cell>
      <fo:block font-size="10pt" >
		Manufacturer's Serial Number:
		</fo:block>
    </fo:table-cell>

   <fo:table-cell>
      <fo:block font-size="10pt" >
		<xsl:value-of select = "mfserial" />
		</fo:block>
    </fo:table-cell>
 </fo:table-row>


  <fo:table-row>
    <fo:table-cell>
      <fo:block font-size="10pt" >
		Manufacturer's Part Number:
		</fo:block>
    </fo:table-cell>

   <fo:table-cell>
      <fo:block font-size="10pt" >
		<xsl:value-of select = "mftype" />
		</fo:block>
    </fo:table-cell>
 </fo:table-row>


  <fo:table-row>
    <fo:table-cell>
      <fo:block font-size="10pt" >
		Technical DB Serial Number:
		</fo:block>
    </fo:table-cell>

   <fo:table-cell>
      <fo:block font-size="10pt" >
		<xsl:value-of select = "lhcid" />
		</fo:block>
    </fo:table-cell>
 </fo:table-row>


  <fo:table-row>
    <fo:table-cell>
      <fo:block font-size="10pt" >
		Operator:
		</fo:block>
    </fo:table-cell>

   <fo:table-cell>
      <fo:block font-size="10pt" >
		<xsl:value-of select = "operator" />
		</fo:block>
    </fo:table-cell>
 </fo:table-row>


  <fo:table-row>
    <fo:table-cell>
      <fo:block font-size="10pt" >
		Date:
		</fo:block>
    </fo:table-cell>

   <fo:table-cell>
      <fo:block font-size="10pt" >
		<xsl:value-of select = "date" /> - <xsl:value-of select = "time" />
		</fo:block>
    </fo:table-cell>
 </fo:table-row>


  <fo:table-row>
    <fo:table-cell>
      <fo:block font-size="10pt" >
		Dynamic Output Voltage test:
		</fo:block>
    </fo:table-cell>

   <fo:table-cell>
      <fo:block font-size="10pt" >
		<xsl:value-of select = "sen" />
		</fo:block>
    </fo:table-cell>
 </fo:table-row>


  <fo:table-row>
    <fo:table-cell>
      <fo:block font-size="10pt" >
		Soak test:
		</fo:block>
    </fo:table-cell>

   <fo:table-cell>
      <fo:block font-size="10pt" >
		<xsl:value-of select = "soak" />
		</fo:block>
    </fo:table-cell>
 </fo:table-row>


  <fo:table-row>
    <fo:table-cell>
      <fo:block font-size="10pt" >
		Mains test:
		</fo:block>
    </fo:table-cell>

   <fo:table-cell>
      <fo:block font-size="10pt" >
		<xsl:value-of select = "mains" />
		</fo:block>
    </fo:table-cell>
 </fo:table-row>



    
</fo:table-body>
</fo:table>
<fo:block font-size="10pt">--------------------------------------------------------</fo:block>
<fo:block font-size="10pt">Note: If not stated otherwise all units are in A, V, W or s</fo:block>
 
     </fo:table-cell>
   
      <fo:table-cell>
      <fo:block font-size="20pt" font-weight="bold">
	<xsl:if test="@result='pass'">
	PASSED
	</xsl:if>
	<xsl:if test="@result='fail'">
	FAILED
	</xsl:if>
		</fo:block>
    </fo:table-cell>
     
     
 </fo:table-row>
    </fo:table-body>
</fo:table>


<xsl:apply-templates  />



<!--		End of the report template -->
 </xsl:template> 

<!-- 		Other Templates  -->






  <xsl:template match = "group[@name='DynamicOutTest']">
        <fo:block font-size="14pt"
        font-weight = "bold" 
        padding-after = "0.5cm"
        padding-before = "0.5cm" >
		<xsl:value-of select="desc" /> - <xsl:value-of select="@result" />
		</fo:block>

<xsl:for-each select = "descendant::syserr">
<fo:block font-size="10pt" font-weight = "bold" >
<xsl:value-of select="." /> - <xsl:value-of select="ancestor::chan/@name" />
</fo:block>
</xsl:for-each>

<xsl:for-each select = "descendant::remark">
<fo:block font-size="10pt">
<xsl:value-of select="ancestor::chan/@name" /><xsl:value-of select="." />
</fo:block>
</xsl:for-each>

<fo:block font-size="10pt" font-weight = "bold" padding-after = "0.2cm"> </fo:block>
<xsl:for-each select = "descendant::comment">
<fo:block font-size="10pt" font-weight = "bold" >
Channel <xsl:value-of select="ancestor::chan/@name" />: <xsl:value-of select="." />
</fo:block>
</xsl:for-each>
<fo:block font-size="10pt" font-weight = "bold" padding-after = "0.2cm"> </fo:block>

<xsl:if test = "boolean(opcomm)">

<fo:table>
<fo:table-column  />
<fo:table-column  />
<fo:table-body><fo:table-row>
<fo:table-cell>
       <fo:block font-size="14pt"
       font-weight = "bold" 
        padding-after = "0.5cm"
        padding-before = "0.5cm" >
		Operator Comment
		</fo:block>

</fo:table-cell><fo:table-cell> 

      <fo:block font-size="14pt"
        padding-after = "0.5cm"
        padding-before = "0.5cm" >
		<xsl:value-of select="opcomm" />
		</fo:block>
</fo:table-cell>
</fo:table-row></fo:table-body></fo:table>

</xsl:if>



<fo:table font-size="10pt" table-layout = "fixed" width = "100%">
<fo:table-column column-width = "10cm" />
<fo:table-column />
<fo:table-body>
<fo:table-row>
<fo:table-cell><fo:block></fo:block></fo:table-cell>
<fo:table-cell><fo:block></fo:block></fo:table-cell>
</fo:table-row>
<fo:table-row>
<fo:table-cell><fo:block>Voltage measured by DVM without load</fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "format-number(delta1,'###0.00')" /></fo:block></fo:table-cell>
</fo:table-row>
<fo:table-row>
<fo:table-cell><fo:block>Voltage measured by DVM under load</fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "format-number(delta2,'###0.00')" /></fo:block></fo:table-cell>
</fo:table-row>
<fo:table-row>
<fo:table-cell><fo:block>PSU output power under load</fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "format-number(power,'###0.00')" /></fo:block></fo:table-cell>
</fo:table-row>
</fo:table-body>
</fo:table>


 </xsl:template>



  <xsl:template match = "group[@name='Soak']">
        <fo:block font-size="14pt"
        font-weight = "bold" 
        padding-after = "0.5cm"
        padding-before = "0.5cm" >
		<xsl:value-of select="desc" /> - <xsl:value-of select="@result" />
		</fo:block>

<xsl:for-each select = "descendant::syserr">
<fo:block font-size="10pt" font-weight = "bold" >
<xsl:value-of select="." /> - <xsl:value-of select="ancestor::chan/@name" />
</fo:block>
</xsl:for-each>

<xsl:for-each select = "descendant::remark">
<fo:block font-size="10pt">
<xsl:value-of select="ancestor::chan/@name" /><xsl:value-of select="." />
</fo:block>
</xsl:for-each>

<fo:block font-size="10pt" font-weight = "bold" padding-after = "0.2cm"> </fo:block>
<xsl:for-each select = "descendant::comment">
<fo:block font-size="10pt" font-weight = "bold" >
Channel <xsl:value-of select="ancestor::chan/@name" />: <xsl:value-of select="." />
</fo:block>
</xsl:for-each>
<fo:block font-size="10pt" font-weight = "bold" padding-after = "0.2cm"> </fo:block>

<xsl:if test = "boolean(opcomm)">

<fo:table>
<fo:table-column  />
<fo:table-column  />
<fo:table-body><fo:table-row>
<fo:table-cell>
       <fo:block font-size="14pt"
       font-weight = "bold" 
        padding-after = "0.5cm"
        padding-before = "0.5cm" >
		Operator Comment
		</fo:block>

</fo:table-cell><fo:table-cell> 

      <fo:block font-size="14pt"
        padding-after = "0.5cm"
        padding-before = "0.5cm" >
		<xsl:value-of select="opcomm" />
		</fo:block>
</fo:table-cell>
</fo:table-row></fo:table-body></fo:table>

</xsl:if>



<fo:table font-size="10pt" table-layout = "fixed" width = "100%">
<fo:table-column column-width = "4cm" />
<fo:table-column />
<fo:table-body>
<fo:table-row>
<fo:table-cell><fo:block></fo:block></fo:table-cell>
<fo:table-cell><fo:block></fo:block></fo:table-cell>
</fo:table-row>
<fo:table-row>
<fo:table-cell><fo:block>Store Interval (s)</fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "Time" /></fo:block></fo:table-cell>
</fo:table-row>
</fo:table-body>
</fo:table>

<fo:block font-size="10pt">Wpsu = PSU output power (measured by PSU)</fo:block>
<fo:block font-size="10pt">Vdvm = PFC output voltage (measured by DVM)</fo:block>

<fo:table font-size="10pt" table-layout = "fixed" width = "100%">
<fo:table-column />
<fo:table-column />
<fo:table-column />
<fo:table-body>
<fo:table-row>
<fo:table-cell><fo:block>Time</fo:block></fo:table-cell>
<fo:table-cell><fo:block>Wpsu</fo:block></fo:table-cell>
<fo:table-cell><fo:block>Vdvm</fo:block></fo:table-cell>
</fo:table-row>
<xsl:for-each select = "chan">
<xsl:for-each select = "group">
<fo:table-row>
<fo:table-cell><fo:block><xsl:value-of select = "parent::*/@val" /></fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "format-number(ipower,'###0.00')" /></fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "format-number(evolt,'###0.00')" /></fo:block></fo:table-cell>
</fo:table-row>
</xsl:for-each>
</xsl:for-each>
</fo:table-body>
</fo:table>



<fo:block font-size="10pt" padding-before = "0.5cm">Min-Max values</fo:block>

<fo:table font-size="10pt" table-layout = "fixed" width = "100%">
<fo:table-column column-width = "5cm" />
<fo:table-column />
<fo:table-body>
<fo:table-row>
<fo:table-cell><fo:block></fo:block></fo:table-cell>
<fo:table-cell><fo:block></fo:block></fo:table-cell>
</fo:table-row>
<fo:table-row>
<fo:table-cell><fo:block>PFC Vmin</fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "format-number(voltmin,'###0.00')" /></fo:block></fo:table-cell>
</fo:table-row>
<fo:table-row>
<fo:table-cell><fo:block>PFC Vmax</fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "format-number(voltmax,'###0.00')" /></fo:block></fo:table-cell>
</fo:table-row>
<fo:table-row>
<fo:table-cell><fo:block>PSU power_min</fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "format-number(powermin,'###0.00')" /></fo:block></fo:table-cell>
</fo:table-row>
<fo:table-row>
<fo:table-cell><fo:block>PSU power_max</fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "format-number(powermax,'###0.00')" /></fo:block></fo:table-cell>
</fo:table-row>
</fo:table-body>
</fo:table>


 </xsl:template>



  <xsl:template match = "group[@name='Mains']">
        <fo:block font-size="14pt"
        font-weight = "bold" 
        padding-after = "0.5cm"
        padding-before = "0.5cm" >
		<xsl:value-of select="desc" /> - <xsl:value-of select="@result" />
		</fo:block>

<xsl:for-each select = "descendant::syserr">
<fo:block font-size="10pt" font-weight = "bold" >
<xsl:value-of select="." /> - <xsl:value-of select="ancestor::chan/@name" />
</fo:block>
</xsl:for-each>

<xsl:for-each select = "descendant::remark">
<fo:block font-size="10pt">
<xsl:value-of select="ancestor::chan/@name" /><xsl:value-of select="." />
</fo:block>
</xsl:for-each>

<fo:block font-size="10pt" font-weight = "bold" padding-after = "0.2cm"> </fo:block>
<xsl:for-each select = "descendant::comment">
<fo:block font-size="10pt" font-weight = "bold" >
Channel <xsl:value-of select="ancestor::chan/@name" />: <xsl:value-of select="." />
</fo:block>
</xsl:for-each>
<fo:block font-size="10pt" font-weight = "bold" padding-after = "0.2cm"> </fo:block>

<xsl:if test = "boolean(opcomm)">

<fo:table>
<fo:table-column  />
<fo:table-column  />
<fo:table-body><fo:table-row>
<fo:table-cell>
       <fo:block font-size="14pt"
       font-weight = "bold" 
        padding-after = "0.5cm"
        padding-before = "0.5cm" >
		Operator Comment
		</fo:block>

</fo:table-cell><fo:table-cell> 

      <fo:block font-size="14pt"
        padding-after = "0.5cm"
        padding-before = "0.5cm" >
		<xsl:value-of select="opcomm" />
		</fo:block>
</fo:table-cell>
</fo:table-row></fo:table-body></fo:table>

</xsl:if>


<fo:block font-size="10pt">-10%: PFC runs with 207V AC input</fo:block>
<fo:block font-size="10pt">+10%: PFC runs with 253V AC input</fo:block>

<fo:table font-size="10pt" table-layout = "fixed" width = "100%">
<fo:table-column column-width = "5cm" />
<fo:table-column />
<fo:table-body>
<fo:table-row>
<fo:table-cell><fo:block></fo:block></fo:table-cell>
<fo:table-cell><fo:block></fo:block></fo:table-cell>
</fo:table-row>
<fo:table-row>
<fo:table-cell><fo:block>PFC Vmin (V)</fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "format-number(min_volt_nom,'###0.00')" /></fo:block></fo:table-cell>
</fo:table-row>
<fo:table-row>
<fo:table-cell><fo:block>PFC Vmax (V)</fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "format-number(max_volt_nom,'###0.00')" /></fo:block></fo:table-cell>
</fo:table-row>
<fo:table-row>
<fo:table-cell><fo:block>PFC Vmean (V)</fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "format-number(mean_volt_nom,'###0.00')" /></fo:block></fo:table-cell>
</fo:table-row>
<fo:table-row>
<fo:table-cell><fo:block>PFC Volt SD</fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "format-number(sdv_volt_nom,'###0.00')" /></fo:block></fo:table-cell>
</fo:table-row>
<fo:table-row>
<fo:table-cell><fo:block>PFC Vmin-10% (V)</fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "format-number(min_volt_minus,'###0.00')" /></fo:block></fo:table-cell>
</fo:table-row>
<fo:table-row>
<fo:table-cell><fo:block>PFC Vmax-10% (V)</fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "format-number(max_volt_minus,'###0.00')" /></fo:block></fo:table-cell>
</fo:table-row>
<fo:table-row>
<fo:table-cell><fo:block>PFC Vmean-10% (V)</fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "format-number(mean_volt_minus,'###0.00')" /></fo:block></fo:table-cell>
</fo:table-row>
<fo:table-row>
<fo:table-cell><fo:block>PFC Volt SD-10%</fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "format-number(sdv_volt_minus,'###0.00')" /></fo:block></fo:table-cell>
</fo:table-row>
<fo:table-row>
<fo:table-cell><fo:block>PFC Vmin+10% (V)</fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "format-number(min_volt_plus,'###0.00')" /></fo:block></fo:table-cell>
</fo:table-row>
<fo:table-row>
<fo:table-cell><fo:block>PFC Vmax+10% (V)</fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "format-number(max_volt_plus,'###0.00')" /></fo:block></fo:table-cell>
</fo:table-row>
<fo:table-row>
<fo:table-cell><fo:block>PFC Vmean+10% (V)</fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "format-number(mean_volt_plus,'###0.00')" /></fo:block></fo:table-cell>
</fo:table-row>
<fo:table-row>
<fo:table-cell><fo:block>PFC Volt SD+10%</fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "format-number(sdv_volt_plus,'###0.00')" /></fo:block></fo:table-cell>
</fo:table-row>
<fo:table-row>
<fo:table-cell><fo:block>PFC Mains input power</fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "format-number(inp,'###0.00')" /></fo:block></fo:table-cell>
</fo:table-row>
<fo:table-row>
<fo:table-cell><fo:block>PSU Output power</fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "format-number(outp,'###0.00')" /></fo:block></fo:table-cell>
</fo:table-row>
<fo:table-row>
<fo:table-cell><fo:block>System efficiency</fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "format-number(eff,'###0.00')" /></fo:block></fo:table-cell>
</fo:table-row>
</fo:table-body>
</fo:table>
 

 </xsl:template>



 
</xsl:stylesheet>


