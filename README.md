# Test Bench reports service

## Warning!

Me, [Luis Rodríguez Fernández](https://phonebook.cern.ch/phonebook/#personDetails/?id=720335), I am not the author of this code!!! I've just made it work...

## What?

Power Supply reports. Application migrated from the phased out **Java Web Hosting Service** to [PaaS Web Application Service](https://cern.service-now.com/service-portal/service-element.do?name=PaaS-Web-App) 

Created on [web.services.cern.ch](https://webservices.web.cern.ch), see [here](https://webservices.web.cern.ch/webservices/Tools/Quotas/default.aspx?SiteName=ph-service-ess-reports) the details. Deployed on openshift.cern.ch. **URL:** https://ph-service-ess-reports.web.cern.ch/ph-service-ess-reports/ViewReport?action_id=228254

## How?

Assuming that you have [eclipse id](https://www.eclipse.org/ide/) and [docker](https://www.docker.com/)

1. Compile and package: [eclipse](https://help.eclipse.org/luna/index.jsp?topic=%2Forg.eclipse.wst.webtools.doc.user%2Ftopics%2Ftwcrewar.html) export war.
2. Build docker container: `docker build -t gitlab-registry.cern.ch/lurodrig/ph-service-ess-reports .`
3. Push to repository: `docker push gitlab-registry.cern.ch/lurodrig/ph-service-ess-reports`
4. Deploy on openshift: `oc new-app --env-file=.env gitlab-registry.cern.ch/lurodrig/ph-service-ess-reports --allow-missing-images` 

[env-file](env-file) should be quite self-explanatory:

```
DB_URL=YOUR_DB_URL 
DB_USER=YOUR_DB_USER 
DB_PASSWORD=YOUR_DB_PASSWORD
```

In openshift these settings are environment variables of the deployment. You can find an example [here](https://cern.service-now.com/service-portal/article.do?n=KB0004506)
 
### Any Issue?

Please contact [PaaS Web Application Service](https://cern.service-now.com/service-portal/service-element.do?name=PaaS-Web-App) support.

### Good to know

1. The application is visible from the CERN campus network (intranet). If you want to make it public you can change the visibility setting from [here](https://webservices.web.cern.ch/webservices/Tools/Permissions/?SiteName=ph-service-ess-reports)

### What's next?

**The sky is the limit...** but perhaps just do little things like removing the **libraries** from [WebContent/WEB-INF/lib](WebContent/WEB-INF/lib) using some tool like [maven](https://maven.apache.org/) or [graddle](https://gradle.org/) would be enough. To store binaries in the repository usually is not a good idea :).




